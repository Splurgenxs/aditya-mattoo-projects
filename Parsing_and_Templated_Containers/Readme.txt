
Project 1:
�	Load the data from the three files and store them into a vector, a list, and a map
o	Decide on a structure for your �person� class CPerson from the USCen.7z file
?	First name
?	Last name
?	Other data (maybe the numbers in the files)
?	(Suggestion) unique identifier, like Social Insurance Number, student number, account number, customer number, etc. (you may want something like this to index your map. Or maybe you don�t)
�	You decide on how you want to �look up� the data (first name, last name, both, etc.)
�	Then:
o	Look up (find) all people by first name
o	Look up (find) all people by last name
o	Sort the list alphabetically (last, then first)
o   do this for all three containers
�	Replace your containers with your own (that you wrote yourself. In other words, you are to create your own vector, list, and map, replacing the STL versions)

Project 2:
�	Performance: 
o	Show me the timing (HRTimer, etc.) of the various calls:
?	Loading the data
?	Searching the data
?	Sorting the data
o	Show me how much memory you used (Operating System calls) of the various calls:
?	Loading the data
?	Searching the data
?	Sorting the data
�	You�d do this for both the STL containers and you own. 
Is there a difference (very likely there is)?


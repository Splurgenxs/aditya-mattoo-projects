#ifndef _map_myClass_
#define _map_myClass_

#include <cstdlib>
#include <iostream>

//BST MAP ATTEMPT FOR MAP CONTAINER---->

template <class Item, class Key>
struct node
{
	node(){};

	Key key;
	Item nData;

	node* right;
	node* left;
};

static unsigned int depth = 0;

template <class Item , class Key>
class Map_MyContainer
{

private:
	//------------------------Node Structure---------------------------
	/*	Key key;
		Item nData;

		Map_MyContainer<Item, Key>* right;
		Map_MyContainer<Item, Key>* left;*/
	//------------------------rootnode---------------------------------
	//	Map_MyContainer<Item, Key>* rootNode;
		node<Item, Key>* rootNode;
	//-----------------------------------------------------------------
		size_t size_Counter;
	//------------------------BST New node-----------------------------
	node<Item, Key>* New_Node(Item Idata, Key Kdata)
	{
		node<Item, Key>* new_NodeObj = new  node<Item, Key>();
		new_NodeObj->nData = Idata;
		new_NodeObj->key = Kdata;

		new_NodeObj->left = NULL;
		new_NodeObj->right = NULL;//-----Setting as leaf;

		return new_NodeObj;
	}
	
	
	//--------------------------BST_Insert---------------------------------------
	void Bst_Insert(node<Item, Key>*& nodePtr, Item itemA, Key keyA, std::string type)
	{
		depth++;
		if (nodePtr == NULL)
		{
			nodePtr = New_Node(itemA, keyA);
			if (type == "null")
			{
				nodePtr = nodePtr;
			}
			else if (type == "right")
			{
				nodePtr->right = nodePtr->right;
			}
			else if (type == "left")
			{
				nodePtr->left = nodePtr->left;
			}
		}
		else if (keyA <= nodePtr->key)
		{
			Bst_Insert(nodePtr->left, itemA, keyA, "left");
		}
		else
		{
			/*nodePtr->right[k] = nodePtr(itemA, keyA);*/
			Bst_Insert(nodePtr->right, itemA, keyA, "right");
		}
		depth--;
	}

//--------------------------------------------------------------

public: 
	Map_MyContainer()
	{
		rootNode = NULL;
		size_Counter = NULL;
	}
	~Map_MyContainer()
	{
		
	}



	node<Item, Key>* get_RootNode()
		{return rootNode;}
	//----
		Key get_key()
		{return key;}
		Item get_data()
		{return nData;}
	//----
	//-----------------------------INSERT----------------------------------
		node<Item, Key>* Insert(Item itemA, Key keyA)
			{
				size_Counter = size_Counter + 1;

				 Bst_Insert(rootNode, itemA, keyA, "null");
				 return rootNode;
			}
	 //----------------------------MAP SIZE---------------------------------
	unsigned int size()
	 {
		 if (rootNode == NULL)
		 {
			 return 0;
		 }
		 /*return(map_size(rootNode->left) + map_size(rootNode->right) + 1);*/
		 return size_Counter;
	 }

	 //---------------------------SEARCH-----------------------------------

	 Item lookup_M(node<Item, Key>* rootNode, Key s_Key)
		{
			if (rootNode == NULL)
			{
				/*return false;*/
				std::cout << "Map is empty" << std::endl;
			}
			else if (rootNode->key == s_Key)
			{
				return rootNode->nData;
			}
			else if (s_Key <= rootNode->key )
			 {
				return lookup_M(rootNode->left, s_Key);
			 }
			else
			{
				return lookup_M(rootNode->right, s_Key);
			}
		}
    //-----------------------------------------------------------------------
};

#endif

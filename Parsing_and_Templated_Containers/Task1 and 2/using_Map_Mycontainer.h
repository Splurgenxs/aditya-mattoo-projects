#pragma once


#ifndef _usingMyMapp_
#define _usingMyMapp_
  

#include <iostream>
#include <string>
#include <map>
#include <fstream>
#include <istream>


#include "Name_Sensus_C.h"
#include "sensus_People.h"

#include "Map_MyContainer.h"


#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <algorithm>  //sorts


class using_Map_Mycontainer
{
public:

	using_Map_Mycontainer();
	~using_Map_Mycontainer();

	void store_in_Map();
	void make_People();


	void Lookup();



	void sort_by_First_OR_LastNames();

private:

	Map_MyContainer< Name_Sensus_C*, int> Map_name_Sensus;
	Map_MyContainer< sensus_People, int> Map_people;


	void nameLookupF(std::string firstName);
	void nameLookupL(std::string lastname);


	std::string firstname, lastname;
	char typeSelection;

	//vector for storing Map data for sorting ===

	std::vector<sensus_People> pepl_MApVector;

	void swap(sensus_People r, sensus_People s);
		void bubble_Sort(std::string nameType);

};

#endif
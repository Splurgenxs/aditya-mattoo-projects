#include "MapGem.h"


MapGem::MapGem()
{

}
MapGem::~MapGem()
{

}

void MapGem::store_in_Map()
{
	std::ifstream Mnfile;//open file
	std::ifstream Fnfile;
	std::ifstream usLastN;
	Mnfile.open("USCen/dist.male.first.txt");
	Fnfile.open("USCen/dist.female.first.txt");
	usLastN.open("USCen/US_LastNames.txt");

	for (int index = 0; index < 88799; index++)//index size is '88799' just to circumvent pushing back later.
	{//cout << "Reading from the file" << endl;
		//cout << "Reading from the file" << endl;
		
		Name_Sensus_C temCon;

		Mnfile >> temCon.Male_Name;
		Mnfile >> temCon.M_C_fequency;
		Mnfile >> temCon.M_F_frequency;
		Mnfile >> temCon.m_Ind;

		Fnfile >> temCon.Female_Name;
		Fnfile >> temCon.F_C_fequency;
		Fnfile >> temCon.F_F_frequency;
		Fnfile >> temCon.F_Ind;

		usLastN >> temCon.Last_Name;
		usLastN >> temCon.L_C_fequency;
		usLastN >> temCon.L_F_frequency;
		usLastN >> temCon.L_Indl;

		Map_name_Sensus[index] = temCon;
	}

	Mnfile.close();
	Fnfile.close();
	usLastN.close();
}

void MapGem::make_People()
{
	//// Timer / memory
	//timeObj.Start();
	//timeObj.windows_MemeFunctions();
	////-------------------------------


	//store_in_Map(); 
	int i = 0;
	
	while (i < 10000)
	{
		for (std::map<int, Name_Sensus_C>::const_iterator mapIter = Map_name_Sensus.begin(); mapIter != Map_name_Sensus.end();)
		{
			sensus_People p_container;

			srand(i);

			//int mindex, findex, lindex,
			//mindex = rand() % 1219 + 0;
			//findex = rand() % 4275 + 0;
			//lindex = rand() % 88799 + 0;*/ //using and interator means i cant seem to find
			//a way to randomise based on index values.

			int genSelect;
			genSelect = rand() % 2 + 1;

			if (genSelect == 1)
			{
				p_container.first_Name = mapIter->second.Male_Name;
				p_container.last_Name = mapIter->second.Last_Name;
				Map_people[i] = p_container;
			}
			else if (genSelect == 2)
			{
				p_container.first_Name = mapIter->second.Female_Name;
				p_container.last_Name = mapIter->second.Last_Name;

				Map_people[i] = p_container;
			}

			i++;
			++mapIter;
		}

	}

	//--------------------------------------------------------
	/*timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;*/

}


void MapGem::nameLookupF(std::string firstName)
{
	
	// Timer / memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------


	std::map<int , sensus_People>matchingNamesF;
	int i = 0;
	for (std::map<int, sensus_People>::iterator Iter = Map_people.begin(); Iter != Map_people.end();)
	{
		std::string tempFirstName = Iter->second.first_Name;
		
		if (firstName == tempFirstName)
		{ 
			matchingNamesF[i] = Iter->second;
			i++;
		}
		Iter++;
	} 
	
	
	for (int index = 0; index < matchingNamesF.size(); index++)
	{
		std::cout << matchingNamesF[index].first_Name << "_" << matchingNamesF[index].last_Name << std::endl;
	}

	if (matchingNamesF.size() == 0)
	{
		std::cout << "Sorry no Matches" << std::endl;
	}

	//--------------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;

}

void MapGem::nameLookupL(std::string lastname)
{

	// Timer / memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------


	std::map<int, sensus_People>matchingNamesL;
	int ind = 0;
	for (std::map<int, sensus_People>::iterator Iter = Map_people.begin(); Iter != Map_people.end();)
	{
		std::string tempLastname = Iter->second.last_Name;

		if (lastname == tempLastname)
		{
			matchingNamesL[ind] = Iter->second;

			ind++;
		}
		Iter++;
	}

	for (int index = 0; index < matchingNamesL.size(); index++)
	{
		std::cout << matchingNamesL[index].first_Name << "_" << matchingNamesL[index].last_Name << std::endl;
	}

	if (matchingNamesL.size() == 0)
	{
		std::cout << "Sorry no Matches" << std::endl;
	}

	//--------------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;


}

void MapGem::Lookup()
{
	std::cout << "Press 'F' to check for first names or Press 'L' to check for last names" << std::endl;
	std::cin >> typeSelection;
	if (typeSelection == 'F')
	{
		std::cout << "Lookup Names" << std::endl;
		std::cout << "Please input First name all in Uppercase" << std::endl;
		std::cout << "First name ->"; std::cin >> firstname;
		/*std::cout << "Last name ->"; std::cin >> lastname;*/
		std::cout << std::endl;
		nameLookupF(firstname);
	}

	else if (typeSelection == 'L')
	{
		std::cout << "Lookup Names" << std::endl;
		std::cout << "Please input Last name all in Uppercase" << std::endl;
		std::cout << "Last name ->"; std::cin >> lastname;
		/*std::cout << "Last name ->"; std::cin >> lastname;*/
		//std::cout << std::endl;
		nameLookupL(lastname);
	}
}

bool nameSortLogic(sensus_People C1_people, sensus_People c2_people)
{
	return(C1_people.first_Name < c2_people.first_Name);
}

void MapGem::sort_People_by_FirstNames()
{

	// Timer / memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------

	int indexer = 0 ;
	for (std::map<int, sensus_People>::iterator Iter = Map_people.begin(); Iter != Map_people.end(); ++Iter)
	{
		pepl_MApVector.push_back(Iter->second);
		indexer++;
	}
		std::sort(pepl_MApVector.begin(), pepl_MApVector.end(), nameSortLogic);

		for (int index = 0; index < pepl_MApVector.size(); index++)
		{
			//std::cout << pepl_MApVector[index].first_Name;
			//std::cout << "_" << pepl_MApVector[index].last_Name<< std::endl;
			Map_people[index] = pepl_MApVector[index];
		}

		//--------------------------------------------------------
		//timeObj.windows_MemeFunctions();
		//timeObj.Stop();
		//std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;
}
#ifndef _ListGem_
#define _ListGem_


#include <iostream>
#include <string>
#include <list>
#include <fstream>
#include <istream>
#include <algorithm>  //sort

#include "Name_Sensus_C.h"
#include "sensus_People.h"


#include "Timer_mem.h"


class ListGem
{
public:
	ListGem();
	~ListGem();

	
	void store_in_list();
	void printInventory();
	void make_People();

	void Lookup();
	void nameLookupF(std::string firstName);
	void nameLookupL(std::string lastname);

	void sort_People_by_FirstNames();

private:

	std::list< Name_Sensus_C*> list_name_Sensus;
	std::list< sensus_People*> people_list;

	int incre;

	std::string firstname, lastname;
	char typeSelection;

	CHRTimer timeObj;
};

#endif
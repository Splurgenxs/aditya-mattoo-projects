#ifndef _VectorMy_Container_
#define _VectorMy_Container_


#include <string>

//================Vector Template===================//

template<typename T>
class Vector_MyContainer
{
private:

	T* m_array;
	unsigned int maxsize, Vsize;

public:

	Vector_MyContainer()
	{
		maxsize = 20;
		m_array = new T[maxsize];
		Vsize = 0;
	}

	~Vector_MyContainer()
	{
		delete[] m_array;
	}

	void push_back(T i)
	{
		if (Vsize + 1 > maxsize)
		{
			alloc_new();
		}
		
		m_array[Vsize] = i;
		Vsize++;
	}

	T operator [](int i)
	{
		return m_array[i];
	}

	T at(int i)
	{
		if (i < Vsize)
		{
			return m_array[i];
		}
		throw 10;
	}

	int size()
	{
		return Vsize;
	}

	void alloc_new()
	{
		maxsize = Vsize * 2;
		T* tmp = new T[maxsize];

		for (int i = 0; i < Vsize; i++)
		{
			tmp[i] = m_array[i];
		}
		
		delete[] m_array;
		m_array = tmp;
	}

	int begin()
	{
		return 0;
	}


};
#endif


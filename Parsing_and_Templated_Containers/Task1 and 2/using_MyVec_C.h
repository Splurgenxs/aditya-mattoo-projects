#pragma once
#ifndef _usingMyVec_
#define _usingMyVec_

#include <string>
#include <iostream>

#include "Vector_MyContainer.h"
#include "Name_Sensus_C.h"
#include "sensus_People.h"


#include "Timer_mem.h"

class using_MyVec_C
{
public:

	using_MyVec_C();
	~using_MyVec_C();


	void store_in_Vector();
	void make_People();

	//------
	void Lookup();
	
	//--------------
	void sort_by_First_OR_LastNames();


	//Sort function 
	


private:

	Vector_MyContainer<Name_Sensus_C*> vec_Name_Sensus_C;
	Vector_MyContainer<sensus_People*>people_vector;


	
	/*Vector_MyContainer<sensus_People> merge(Vector_MyContainer<sensus_People> &vec,  Vector_MyContainer<sensus_People>& left,  Vector_MyContainer<sensus_People>& right, std::string nameType);
	Vector_MyContainer<sensus_People> merge_sort(Vector_MyContainer<sensus_People>& vec, std::string nameType);*/

	std::string firstname, lastname;
	char typeSelection;


	void nameLookupF(std::string firstName);
	void nameLookupL(std::string lastname);

	void bubble_Sort(std::string nameType);
	void swap(sensus_People* r, sensus_People* s);

	CHRTimer timeObj;
};


#endif

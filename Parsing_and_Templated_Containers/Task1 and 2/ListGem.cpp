#include "ListGem.h"



ListGem::ListGem()
{
	incre = 0;
}
ListGem::~ListGem()
{
}


void ListGem::store_in_list()
{
	
	std::ifstream Mnfile;//open file
	std::ifstream Fnfile;
	std::ifstream usLastN;
	Mnfile.open("USCen/dist.male.first.txt");
	Fnfile.open("USCen/dist.female.first.txt");
	usLastN.open("USCen/US_LastNames.txt");

	//for (int index = 0; index < 1219; index++)
	for (int index = 0; index < 88799; index++)//index size is '88799' just to circumvent pushing back later.
	{//cout << "Reading from the file" << endl;
		Name_Sensus_C* temCon = new  Name_Sensus_C();

		Mnfile >> temCon->Male_Name;
		Mnfile >> temCon->M_C_fequency;
		Mnfile >> temCon->M_F_frequency;
		Mnfile >> temCon->m_Ind;

		Fnfile >> temCon->Female_Name;
		Fnfile >> temCon->F_C_fequency;
		Fnfile >> temCon->F_F_frequency;
		Fnfile >> temCon->F_Ind;

		usLastN >> temCon->Last_Name;
		usLastN >> temCon->L_C_fequency;
		usLastN >> temCon->L_F_frequency;
		usLastN >> temCon->L_Indl;


		list_name_Sensus.push_back(temCon);

		/*	std::cout << vec_Mname_Sensus[index]->Male_Name << std::endl;*/
	}

	Mnfile.close();
	usLastN.close();
	Fnfile.close();

	/*printInventory();*/
	//TImer

}

void ListGem::make_People()
{
	//Timer/memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------

	//store_in_list();

	timeObj.Start();
	for (std::list<Name_Sensus_C*>::iterator it = list_name_Sensus.begin(); it != list_name_Sensus.end(); ++it)
	{
		sensus_People* p_container = new sensus_People();
		
		
		incre++;
		
		srand(incre);
		if (incre < 10000)
		{
			int mindex, findex, lindex, genSelect;
			mindex = rand() % 1219 + 0;
			findex = rand() % 4275 + 0;
			lindex = rand() % 88799 + 0;

			genSelect = rand() % 2 + 1;

			if (genSelect == 1)
			{
				p_container->first_Name = (*it)->Male_Name;
				p_container->last_Name = (*it)->Last_Name;
				people_list.push_back(p_container);
			}
			else if (genSelect == 2)
			{
				p_container->first_Name = (*it)->Female_Name;
				p_container->last_Name = (*it)->Last_Name;
				people_list.push_back(p_container);
			}
		}
		/*std::cout << people_vector[index]->first_Name << " " << people_vector[index]->last_Name << std::endl;*/
	}



	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;
}


void ListGem::nameLookupF(std::string firstName)
{
	std::list<sensus_People*>matchingNames;

	for (std::list<sensus_People*>::iterator it = people_list.begin(); it != people_list.end(); ++it)
	{
		std::string tempFirstName = (*it)->first_Name;

		if (firstName == tempFirstName)
		{
			matchingNames.push_back(*it);
		}
	}

	for (std::list<sensus_People*>::iterator it = matchingNames.begin(); it != matchingNames.end(); ++it)
	{
		std::cout << (*it)->first_Name << "_" << (*it)->last_Name << std::endl;
	}

	if (matchingNames.size() == 0)
	{
		std::cout << "Sorry no Matches" << std::endl;
	}
}

void ListGem::nameLookupL(std::string lastname)
{
	std::list<sensus_People*>matchingNames;

	for (std::list<sensus_People*>::iterator it = people_list.begin(); it != people_list.end(); ++it)
	{
		std::string templastName = (*it)->last_Name;

		if (lastname == templastName)
		{
			matchingNames.push_back(*it);
		}
	}
	for (std::list<sensus_People*>::iterator it = matchingNames.begin(); it != matchingNames.end(); ++it)
	{
		std::cout << (*it)->first_Name << "_" << (*it)->last_Name << std::endl;
	}

	if (matchingNames.size() == 0)
	{
		std::cout << "Sorry no Matches" << std::endl;
	}
}

void ListGem::Lookup()
{
	//Timer/memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------

	timeObj.Start();
	std::cout << "Press 'F' to check for first names or Press 'L' to check for last names" << std::endl;
	std::cin >> typeSelection;
	if (typeSelection == 'F')
	{
		std::cout << "Lookup Names" << std::endl;
		std::cout << "Please input First name all in Uppercase" << std::endl;
		std::cout << "First name ->"; std::cin >> firstname;
		/*std::cout << "Last name ->"; std::cin >> lastname;*/
		std::cout << std::endl;
		nameLookupF(firstname);
	}
	else if (typeSelection == 'L')
	{
		std::cout << "Lookup Names" << std::endl;
		std::cout << "Please input Last name all in Uppercase" << std::endl;
		std::cout << "Last name ->"; std::cin >> lastname;
		/*std::cout << "Last name ->"; std::cin >> lastname;*/
		std::cout << std::endl;
		nameLookupL(lastname);
	}

	//--------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;


}



bool listnameSortLogic(sensus_People* C1_people, sensus_People* c2_people)
{
	return(C1_people->first_Name < c2_people->first_Name);
}

void ListGem::sort_People_by_FirstNames()
{
	//Timer/memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------

	timeObj.Start();
	
	people_list.sort(listnameSortLogic);
	for (std::list<sensus_People*>::iterator it = people_list.begin(); it != people_list.end(); ++it)
	{
		std::cout << (*it)->first_Name << "_" << (*it)->last_Name << std::endl;
	}

	timeObj.Stop();
	std::cout << timeObj.GetElapsedSeconds() << std::endl;

//----------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;
}

void ListGem::printInventory()
	{
		for (std::list<Name_Sensus_C*>::iterator it = list_name_Sensus.begin(); it != list_name_Sensus.end(); ++it)
		{
			std::cout << (*it)->Male_Name << std::endl;
		}
	}

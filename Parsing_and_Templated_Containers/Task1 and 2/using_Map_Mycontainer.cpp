#include "using_Map_Mycontainer.h"

#include <iostream>
#include <fstream>
#include <istream>

#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <algorithm>  //sort

using_Map_Mycontainer::using_Map_Mycontainer()
{

}
using_Map_Mycontainer::~using_Map_Mycontainer()
{

}


void using_Map_Mycontainer::store_in_Map()
{
	std::ifstream Mnfile;//open file
	std::ifstream Fnfile;
	std::ifstream usLastN;
	Mnfile.open("USCen/dist.male.first.txt");
	Fnfile.open("USCen/dist.female.first.txt");
	usLastN.open("USCen/US_LastNames.txt");

	for (size_t index = 0; index < 80000; index++)
	{
		//cout << "Reading from the file" << endl;

		Name_Sensus_C* temCon = new Name_Sensus_C();//*/

		Mnfile >> temCon->Male_Name;
		Mnfile >> temCon->M_C_fequency;
		Mnfile >> temCon->M_F_frequency;
		Mnfile >> temCon->m_Ind;

		Fnfile >> temCon->Female_Name;
		Fnfile >> temCon->F_C_fequency;
		Fnfile >> temCon->F_F_frequency;
		Fnfile >> temCon->F_Ind;

		usLastN >> temCon->Last_Name;
		usLastN >> temCon->L_C_fequency;
		usLastN >> temCon->L_F_frequency;
		usLastN >> temCon->L_Indl;
		
		std::cout << index << std::endl;
	
		Map_name_Sensus.Insert(temCon, index);
	}
	
		
	/*for (int index = 0; index < Map_name_Sensus.size(); index++)
	{
		std::cout << Map_name_Sensus.lookup_M(Map_name_Sensus.get_RootNode(), index).Female_Name;
	}*/

	Mnfile.close();
	Fnfile.close();
	usLastN.close();
}

void using_Map_Mycontainer::make_People()
{
	store_in_Map();
	int i = 0;

	while (i < 1000)
	{
		/*for (std::map<int, Name_Sensus_C>::const_iterator mapIter = Map_name_Sensus.begin(); mapIter != Map_name_Sensus.end();)*/
			for (int index = 0; index < Map_name_Sensus.size() ; index++)
		{
				sensus_People p_container ;

			srand(i);

			//int mindex, findex, lindex,
			//mindex = rand() % 1219 + 0;
			//findex = rand() % 4275 + 0;
			//lindex = rand() % 88799 + 0;*/ //using and interator means i cant seem to find
			//a way to randomise based on index values.

			int genSelect;
			genSelect = rand() % 2 + 1;

			if (genSelect == 1)
			{
				p_container.first_Name = Map_name_Sensus.lookup_M(Map_name_Sensus.get_RootNode(), index)->Male_Name;
				p_container.last_Name = Map_name_Sensus.lookup_M(Map_name_Sensus.get_RootNode(), index)->Last_Name;
				Map_people.Insert(p_container,i);
			}
			else if (genSelect == 2)
			{
				p_container.first_Name = Map_name_Sensus.lookup_M(Map_name_Sensus.get_RootNode(), index)->Female_Name;
				p_container.last_Name = Map_name_Sensus.lookup_M(Map_name_Sensus.get_RootNode(), index)->Last_Name;
				Map_people.Insert(p_container, i);
			}
		}
	}
}
//
void using_Map_Mycontainer::nameLookupF(std::string firstName)
{

	Map_MyContainer<sensus_People,int> matchingNamesF;

	for (int ind = 0; ind < Map_people.size(); ind++)
	{
		std::string tempFirstName = Map_people.lookup_M(Map_people.get_RootNode(), ind).first_Name;

		if (firstName == tempFirstName)
		{
			matchingNamesF.Insert(Map_people.lookup_M(Map_people.get_RootNode(), ind), ind);
			
		}
		
	}

	for (int index = 0; index < matchingNamesF.size(); index++)
	{
		std::cout << matchingNamesF.lookup_M(matchingNamesF.get_RootNode(), index).first_Name << "_" << matchingNamesF.lookup_M(matchingNamesF.get_RootNode(), index).last_Name << std::endl;
	}

	if (matchingNamesF.size() == 0)
	{
		std::cout << "Sorry no Matches" << std::endl;
	}

}

void using_Map_Mycontainer::nameLookupL(std::string lastname)
{
	Map_MyContainer<sensus_People, int> matchingNamesF;
	int i = 0;

	//for (std::map<int, sensus_People>::iterator Iter = Map_people.begin(); Iter != Map_people.end();)
	for (int ind = 0; ind < Map_people.size(); ind++)
	{
		std::string tempLastName = Map_people.lookup_M(Map_people.get_RootNode(), ind).last_Name;

		if (lastname == tempLastName)
		{
			matchingNamesF.Insert(Map_people.lookup_M(Map_people.get_RootNode(), ind), ind);
		}
	}


	for (int index = 0; index < matchingNamesF.size(); index++)
	{
		std::cout << matchingNamesF.lookup_M(matchingNamesF.get_RootNode(), index).first_Name << "_" << matchingNamesF.lookup_M(matchingNamesF.get_RootNode(), index).last_Name << std::endl;
	}

	if (matchingNamesF.size() == 0)
	{
		std::cout << "Sorry no Matches" << std::endl;
	}
}

void using_Map_Mycontainer::Lookup()
{
	std::cout << "Press 'F' to check for first names or Press 'L' to check for last names" << std::endl;
	std::cin >> typeSelection;
	if (typeSelection == 'F')
	{
		std::cout << "Lookup Names" << std::endl;
		std::cout << "Please input First name all in Uppercase" << std::endl;
		std::cout << "First name ->"; std::cin >> firstname;
		/*std::cout << "Last name ->"; std::cin >> lastname;*/
		std::cout << std::endl;
		nameLookupF(firstname);
	}

	else if (typeSelection == 'L')
	{
		std::cout << "Lookup Names" << std::endl;
		std::cout << "Please input Last name all in Uppercase" << std::endl;
		std::cout << "Last name ->"; std::cin >> lastname;
		/*std::cout << "Last name ->"; std::cin >> lastname;*/
		std::cout << std::endl;
		nameLookupL(lastname);
	}
}


void using_Map_Mycontainer::swap(sensus_People r, sensus_People s)
{
	sensus_People temp = r;
	r = s;
	s = temp;
	return;
}

void using_Map_Mycontainer::bubble_Sort(std::string nameType)
{


	if (nameType == "FIRST")
	{
		for (int xindex = 0; xindex < Map_people.size(); xindex++)
		{
			for (int yindex = xindex + 1; yindex < Map_people.size(); yindex++)
			{
				if (Map_people.lookup_M(Map_people.get_RootNode(), xindex).first_Name > Map_people.lookup_M(Map_people.get_RootNode(), yindex).first_Name)
				{
					swap(Map_people.lookup_M(Map_people.get_RootNode(), xindex), Map_people.lookup_M(Map_people.get_RootNode(), xindex));

				}
			}
		}
	}

	if (nameType == "LAST")
	{
		for (int xindex = 0; xindex < Map_people.size(); xindex++)
		{
			for (int yindex = xindex + 1; yindex < Map_people.size(); yindex++)
			{
				if (Map_people.lookup_M(Map_people.get_RootNode(), xindex).first_Name > Map_people.lookup_M(Map_people.get_RootNode(), yindex).last_Name)
				{
					swap(Map_people.lookup_M(Map_people.get_RootNode(), xindex), Map_people.lookup_M(Map_people.get_RootNode(), xindex));
				}
			}
		}
	}

	for (int index = 0; index < Map_people.size(); index++)
	{
		std::cout << Map_people.lookup_M(Map_people.get_RootNode(), index).first_Name << "_" << Map_people.lookup_M(Map_people.get_RootNode(), index).last_Name << std::endl;
	}

}
//
void using_Map_Mycontainer::sort_by_First_OR_LastNames()
{

	std::cout << "please enter 'FIRST' or 'LAST' to Sort the names by First and last name respectively " << std::endl;
	std::string temp;
	std::cin >> temp;

	bubble_Sort(temp);
}
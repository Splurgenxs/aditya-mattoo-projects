#pragma once

#include <iostream>
#include <string>
#include <map>
#include <fstream>
#include <istream>


#include "Name_Sensus_C.h"
#include "sensus_People.h"

#include "Timer_mem.h"


#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <algorithm>  //sorts


class MapGem
{
public:
	MapGem();
	~MapGem();

	void store_in_Map();
	void make_People();


	void Lookup();
	void nameLookupF(std::string firstName);
	void nameLookupL(std::string lastname);


	void sort_People_by_FirstNames();

private:

	std::map< int, Name_Sensus_C > Map_name_Sensus;
	std::map < int, sensus_People> Map_people;
	


	std::string firstname, lastname;
	char typeSelection;

	//vector for storing Map data for sorting ===

	std::vector<sensus_People> pepl_MApVector;



	CHRTimer timeObj;
};


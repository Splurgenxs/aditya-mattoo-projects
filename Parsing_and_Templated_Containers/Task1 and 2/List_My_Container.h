#ifndef _MylistVec_
#define _MylistVec_

#include <iostream>

template<typename T>
class List_MyContainer
{

private :

	long int list_size;

//-----------------NodeStruct----
	T data;
	List_MyContainer* next_node;
//-------------------------------
	List_MyContainer* head;
	List_MyContainer* current;
	List_MyContainer* temp;


public:
	List_MyContainer()
	{
		List_MyContainer* head = NULL;
		List_MyContainer* current = NULL;
		List_MyContainer* temp = NULL;

		list_size = 0;

	}
	~List_MyContainer()
	{
	/*	delete 	 head;
		delete  current;
		delete   temp;*/
	}
	
	void push_back(T addData)
	{
		this->list_size = list_size + 1;
	

		List_MyContainer* nodePtr = new List_MyContainer;
		nodePtr->next_node = NULL;
		nodePtr->data = addData;
	
		if (head != NULL)
		{
			current = head;

			while (current->next_node != NULL)
			{
				current = current->next_node;
			}

			current->next_node = nodePtr;
		}
		else
		{
			head = nodePtr;
		}
	}

	void DeleteNode(T delData)
	{
		List_MyContainer* delptr = NULL;
		temp = head;
		current = head;

		while (current != NULL && current->data != delData)
		{
			temp = current;
			current = current->next_node;
		}

		if (current == NULL)
		{
			std::cout << delData << "was not in the list " << std::endl;
			delete delptr;
		}
		else
		{
			delptr = current;
			current = current->next_node;
			temp->next_node = current;

			delete delptr;

			std::cout << "The Val ->" << delData << "got deleted" << std::endl;
		}
	}

	T Lookup_list(int index)
	{
		current = head;
		
		/*while (current != NULL)*/
		for (int i = 0; i < index; i++)
		{
			/*current->data;*/
			current = current->next_node;
		}
		return current->data;
		
	}

	int size()
	{
		return list_size;
	}

};

#endif
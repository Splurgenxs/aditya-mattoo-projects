#include "using_List_myContainer.h"

#include <iostream>
#include <fstream>
#include <istream>

#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <algorithm>  //sort


using_List_myContainer::using_List_myContainer()
{}
using_List_myContainer::~using_List_myContainer()
{}


void using_List_myContainer::store_in_list()
{
	std::ifstream Mnfile;//open file
	std::ifstream Fnfile;
	std::ifstream usLastN;
	Mnfile.open("USCen/dist.male.first.txt");
	Fnfile.open("USCen/dist.female.first.txt");
	usLastN.open("USCen/US_LastNames.txt");

	//for (int index = 0; index < 1219; index++)
	for (int index = 0; index < 88799; index++)//index size is '88799' just to circumvent pushing back later.
	{//cout << "Reading from the file" << endl;
		Name_Sensus_C* temCon = new  Name_Sensus_C();

		Mnfile >> temCon->Male_Name;
		Mnfile >> temCon->M_C_fequency;
		Mnfile >> temCon->M_F_frequency;
		Mnfile >> temCon->m_Ind;

		Fnfile >> temCon->Female_Name;
		Fnfile >> temCon->F_C_fequency;
		Fnfile >> temCon->F_F_frequency;
		Fnfile >> temCon->F_Ind;

		usLastN >> temCon->Last_Name;
		usLastN >> temCon->L_C_fequency;
		usLastN >> temCon->L_F_frequency;
		usLastN >> temCon->L_Indl;

		My_list_name_Sensus.push_back(temCon);
		

	}

	Mnfile.close();
	usLastN.close();
	Fnfile.close();

	/*printInventory();*/
}


void using_List_myContainer::make_People()
{

	//Timer/memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------


	store_in_list();

	for (int index = 1; index < My_list_name_Sensus.size(); index++ )
	{
		sensus_People* p_container = new sensus_People();

		
		srand(index);
		if (index < 200)//-------------------Number of people to make;
		{
			int mindex, findex, lindex, genSelect;
			mindex = rand() % 1219 + 0;
			findex = rand() % 4275 + 0;
			lindex = rand() % 88799 + 0;

			genSelect = rand() % 2 + 1;

			if (genSelect == 1)
			{
				p_container->first_Name = My_list_name_Sensus.Lookup_list(index)->Male_Name;
				p_container->last_Name = My_list_name_Sensus.Lookup_list(index)->Last_Name;
				people_mylist.push_back(p_container);
			}
			else if (genSelect == 2)
			{
				p_container->first_Name = My_list_name_Sensus.Lookup_list(index)->Female_Name;
				p_container->last_Name = My_list_name_Sensus.Lookup_list(index)->Last_Name;
				people_mylist.push_back(p_container);
			}
		}
	}

	//--------------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;
}



void using_List_myContainer::nameLookupF(std::string firstName)
{
	//Timer/memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------

	sensus_People* tempContainer = new sensus_People();

	for (int index = 0; index < people_mylist.size(); index++)
	{
		std::string tempFirstName = people_mylist.Lookup_list(index)->first_Name;

		if (firstName == tempFirstName)
		{
			tempContainer->first_Name = people_mylist.Lookup_list(index)->first_Name;
			tempContainer->last_Name = people_mylist.Lookup_list(index)->last_Name;

			matchingNamesF.push_back(tempContainer);
		}
	}

	for (int index = 0; index < matchingNamesF.size(); index++)
	{
		std::cout << matchingNamesF.Lookup_list(index)->first_Name << "_" << matchingNamesF.Lookup_list(index)->last_Name << std::endl;
	}

	if (matchingNamesF.size() == 0)
	{
		std::cout << "Sorry no Matches" << std::endl;
	}

	//--------------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;

}


void using_List_myContainer::nameLookupL(std::string lastname)
{

	//Timer/memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------
	sensus_People* tempContainer = new sensus_People();

	for (int index = 0; index < people_mylist.size(); index++)
	{
		std::string tempLastName = people_mylist.Lookup_list(index)->last_Name;

		if (lastname == tempLastName)
		{
			tempContainer->first_Name = people_mylist.Lookup_list(index)->first_Name;
			tempContainer->last_Name = people_mylist.Lookup_list(index)->last_Name;

			matchingNamesF.push_back(tempContainer);
		}
	}

	for (int index = 0; index < matchingNamesF.size(); index++)
	{
		std::cout << matchingNamesF.Lookup_list(index)->first_Name << " " << matchingNamesF.Lookup_list(index)->last_Name << std::endl;
	}

	if (matchingNamesF.size() == 0)
	{
		std::cout << "Sorry no Matches" << std::endl;
	}

	//--------------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;
}


void using_List_myContainer::Lookup()
{
	std::cout << "Press 'F' to check for first names or Press 'L' to check for last names" << std::endl;
	std::cin >> typeSelection;
	if (typeSelection == 'F')
	{
		std::cout << "Lookup Names" << std::endl;
		std::cout << "Please input First name all in Uppercase" << std::endl;
		std::cout << "First name ->"; std::cin >> firstname;
		/*std::cout << "Last name ->"; std::cin >> lastname;*/
		std::cout << std::endl;
		nameLookupF(firstname);
	}
	else if (typeSelection == 'L')
	{
		std::cout << "Lookup Names" << std::endl;
		std::cout << "Please input Last name all in Uppercase" << std::endl;
		std::cout << "Last name ->"; std::cin >> lastname;
		/*std::cout << "Last name ->"; std::cin >> lastname;*/
		std::cout << std::endl;
		nameLookupL(lastname);
	}

}

void using_List_myContainer::swap(sensus_People* r, sensus_People* s)
{
	sensus_People temp = *r;
	*r = *s;
	*s = temp;
	return;
}

void using_List_myContainer::bubble_Sort(std::string nameType)
{
	//Timer/memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------

	if (nameType == "FIRST")
	{
		for (int xindex = 0; xindex < people_mylist.size(); xindex++)
		{
			for (int yindex = xindex + 1; yindex < people_mylist.size(); yindex++)
			{
				if (people_mylist.Lookup_list(xindex)->first_Name > people_mylist.Lookup_list(yindex)->first_Name)
				{
					swap(people_mylist.Lookup_list(xindex), people_mylist.Lookup_list(yindex));
				}
			}
		}
	}

	if (nameType == "LAST")
	{
		for (int xindex = 0; xindex < people_mylist.size(); xindex++)
		{
			for (int yindex = xindex + 1; yindex < people_mylist.size(); yindex++)
			{
				if (people_mylist.Lookup_list(xindex)->last_Name > people_mylist.Lookup_list(yindex)->last_Name)
				{
					swap(people_mylist.Lookup_list(xindex), people_mylist.Lookup_list(yindex));
				}
			}
		}
	}

	for (int index = 0; index < people_mylist.size(); index++)
	{
		std::cout << people_mylist.Lookup_list(index)->first_Name << "_" << people_mylist.Lookup_list(index)->last_Name << std::endl;
	}

	//--------------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;
}
//
void using_List_myContainer::sort_by_First_OR_LastNames()
{

	std::cout << "please enter 'FIRST' or 'LAST' to Sort the names by First and last name respectively " << std::endl;
	std::string temp;
	std::cin >> temp;

	bubble_Sort(temp);
}

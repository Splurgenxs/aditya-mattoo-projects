#include "using_MyVec_C.h"
#include <fstream>
#include <istream>

#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <algorithm>  //sort


using_MyVec_C::using_MyVec_C()
{
}
using_MyVec_C::~using_MyVec_C()
{
}

void using_MyVec_C::store_in_Vector()
{
	std::ifstream Mnfile;//open file
	std::ifstream Fnfile;
	std::ifstream usLastN;
	Mnfile.open("USCen/dist.male.first.txt");
	Fnfile.open("USCen/dist.female.first.txt");
	usLastN.open("USCen/US_LastNames.txt");

	//for (int index = 0; index < 1219; index++)
	for (int index = 0; index < 88799; index++)
	 {//cout << "Reading from the file" << endl;
		Name_Sensus_C* temCon = new Name_Sensus_C;

		Mnfile >> temCon->Male_Name;
		Mnfile >> temCon->M_C_fequency;
		Mnfile >> temCon->M_F_frequency;
		Mnfile >> temCon->m_Ind;


		Fnfile >> temCon->Female_Name;
		Fnfile >> temCon->F_C_fequency;
		Fnfile >> temCon->F_F_frequency;
		Fnfile >> temCon->F_Ind;

		usLastN >> temCon->Last_Name;
		usLastN >> temCon->L_C_fequency;
		usLastN >> temCon->L_F_frequency;
		usLastN >> temCon->L_Indl;

		vec_Name_Sensus_C.push_back(temCon);	
	}

	Mnfile.close();
	usLastN.close();
	Fnfile.close();
}

void using_MyVec_C::make_People()
{
	// Timer / memory
		timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------

	store_in_Vector();

	for (int index = 0; index < 5; index++)//------>set number of ppl to make
	{
		sensus_People* p_container = new sensus_People;

		srand(index);

		int mindex, findex, lindex, genSelect;
		mindex = rand() % 1219 + 0;
		findex = rand() % 4275 + 0;
		lindex = rand() % 88799 + 0;

		genSelect = rand() % 2 + 1;

		if (genSelect == 1)
		{

			p_container->first_Name = vec_Name_Sensus_C[mindex]->Male_Name;
			p_container->last_Name = vec_Name_Sensus_C[lindex]->Last_Name;
			people_vector.push_back(p_container);
		}
		else if (genSelect == 2)
		{
			p_container->first_Name = vec_Name_Sensus_C[findex]->Female_Name;
			p_container->last_Name = vec_Name_Sensus_C[lindex]->Last_Name;
			people_vector.push_back(p_container);
		}

		std::cout << people_vector[index]->first_Name << " " << people_vector[index]->last_Name << std::endl;
		//std::cout << "......." << std::endl << std::endl;
	}
	//--------------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;
}

void using_MyVec_C::nameLookupF(std::string firstName)
{
	// Timer / memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------


       Vector_MyContainer<sensus_People*>matchingNames;

	for (int index = 0; index < people_vector.size(); index++)
	{
		std::string tempFirstName = people_vector[index]->first_Name;

		if (firstName == tempFirstName)
		{
			matchingNames.push_back(people_vector[index]);
		}
	}

	for (int index = 0; index < matchingNames.size(); index++)
	{
		std::cout << matchingNames[index]->first_Name << "_" << matchingNames[index]->last_Name << std::endl;
	}

	if (matchingNames.size() == 0)
	{
		std::cout << "Sorry no Matches" << std::endl;
	}

	//--------------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;
}

void using_MyVec_C::nameLookupL(std::string lastname)
{

	// Timer / memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------

	std::vector<sensus_People*>matchingNames;

	for (int index = 0; index < people_vector.size(); index++)
	{
		std::string templastName = people_vector[index]->last_Name;

		if (lastname == templastName)
		{
			matchingNames.push_back(people_vector[index]);
		}
	}
	for (int index = 0; index < matchingNames.size(); index++)
	{
		std::cout << matchingNames[index]->first_Name << "_" << matchingNames[index]->last_Name << std::endl;
	}

	if (matchingNames.size() == 0)
	{
		std::cout << "Sorry no Matches" << std::endl;
	}

	//--------------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;

}

void using_MyVec_C::Lookup()
{
	std::cout << "Press 'F' to check for first names or Press 'L' to check for last names" << std::endl;
	std::cin >> typeSelection;
	if (typeSelection == 'F')
	{
		std::cout << "Lookup Names" << std::endl;
		std::cout << "Please input First name all in Uppercase" << std::endl;
		std::cout << "First name ->"; std::cin >> firstname;
		/*std::cout << "Last name ->"; std::cin >> lastname;*/
		std::cout << std::endl;
		nameLookupF(firstname);
	}
	else if (typeSelection == 'L')
	{
		std::cout << "Lookup Names" << std::endl;
		std::cout << "Please input Last name all in Uppercase" << std::endl;
		std::cout << "Last name ->"; std::cin >> lastname;
		/*std::cout << "Last name ->"; std::cin >> lastname;*/
		std::cout << std::endl;
		nameLookupL(lastname);
	}

}

//----------->MERGE SORT FUNCTION NOT IMPLEMENTED<--------------------------
//Vector_MyContainer<sensus_People> using_MyVec_C::merge(Vector_MyContainer<sensus_People> &vec,  Vector_MyContainer<sensus_People>& left,  Vector_MyContainer<sensus_People>& right, std::string nameType)
//{
//	// Fill the resultant vector with sorted results from both vectors
//	Vector_MyContainer<sensus_People> result;
//	unsigned left_it = 0, right_it = 0;
//
//	if (nameType == "FIRST")
//	{
//		
//		while (left_it < left.size() && right_it < right.size())
//		{
//			if (left[left_it].first_Name < right[right_it].first_Name)
//			{
//				result.push_back(left[left_it]);
//				left_it++;
//			}
//			else
//			{
//				result.push_back(right[right_it]);
//				right_it++;
//			}
//		}
//	}
//	else if (nameType == "LAST")
//	{
//		
//		while (left_it < left.size() && right_it < right.size())
//		{
//			if (left[left_it].last_Name < right[right_it].last_Name)
//			{
//				result.push_back(left[left_it]);
//				left_it++;
//			}
//			else
//			{
//				result.push_back(right[right_it]);
//				right_it++;
//			}
//		}
//	}
//
//	// Push the remaining data from both vectors onto the resultant
//	while (left_it < left.size())
//	{
//		result.push_back(left[left_it]);
//		left_it++;
//	}
//
//	while (right_it < right.size())
//	{
//		result.push_back(right[right_it]);
//		right_it++;
//	}
//	//show merge process..
//	int i;
//	for (i = 0; i < result.size(); i++)
//		{
//			std::cout << result[i].first_Name << result[i].last_Name << " ";
//		}
//	// break each line for display purposes..
//	std::cout << "***********" << std::endl;
//
//	//take a source vector and parse the result to it. then return it.  
//	vec = result;
//	return vec;
//}
//
//Vector_MyContainer<sensus_People>using_MyVec_C::merge_sort(Vector_MyContainer<sensus_People>& vec, std::string nameType)
//{
//	// Termination condition: List is completely sorted if it
//	// only contains a single element.
//	if (vec.size() == 1)
//	{
//		return vec;
//	}
//
//	// Determine the location of the middle element of the vector
//	int middle = 0;
//	  middle = (vec.begin() + (vec.size() / 2));
//
//
//
//	  // Store vlues on the left of the vector
//*--->*/  Vector_MyContainer<sensus_People> left;
//	  for (int index = 0; index < middle; index++)
//	  {
//		  left.push_back(vec[index]);
//		 /* std::cout << left[index].first_Name<<std::endl;*/
//	  }
//
//	  // Store value on the right of the vector
//*--->*/  Vector_MyContainer<sensus_People> right;
//	  for (int index = 0; index < vec.size(); index++)
//	  {
//		  right.push_back(vec[index + middle]);
//	  }
//	
//	// Perform a merge sort on the two smaller vectors
//	  
//	  left = merge_sort(left, nameType);
//	  right = merge_sort(right, nameType);
//
//	return merge(vec, left, right, nameType);
//}


void using_MyVec_C::swap(sensus_People* r, sensus_People* s)
{
	sensus_People temp = *r;
	*r = *s;
	*s = temp;
	return;
}

void using_MyVec_C::bubble_Sort(std::string nameType)
{
	// Timer / memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------


	/*std::vector<sensus_People>sorted_vec;*/
	if (nameType == "FIRST")
	{
		for (int xindex = 0; xindex < people_vector.size(); xindex++)
		{
			for (int yindex = xindex+1; yindex < people_vector.size(); yindex++)
			{
				if (people_vector[xindex]->first_Name > people_vector[yindex]->first_Name)
				{
					swap(people_vector[xindex], people_vector[yindex]);
				}
			}
		}
	}

	if (nameType == "LAST")
	{
		for (int xindex = 0; xindex < people_vector.size(); xindex++)
		{
			for (int yindex = xindex + 1; yindex < people_vector.size(); yindex++)
			{

				if (people_vector[xindex]->first_Name > people_vector[yindex]->first_Name)
				{
					swap(people_vector[xindex], people_vector[yindex]);
				}
			}
		}
	}

	for (int index = 0; index < people_vector.size(); index++)
	{
		std::cout << people_vector[index]->first_Name << "_" << people_vector[index]->last_Name << std::endl;
	}

	//--------------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;

	
}

void using_MyVec_C::sort_by_First_OR_LastNames()
{

	std::cout << "please enter 'FIRST' or 'LAST' to Sort the names by First and last name respectively " << std::endl;
	std::string temp;
	std::cin >> temp;
	
	bubble_Sort(temp);
}
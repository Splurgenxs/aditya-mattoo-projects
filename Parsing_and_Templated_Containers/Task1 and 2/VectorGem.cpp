#include "VectorGem.h"


VectorGem::VectorGem()
{
}
VectorGem::~VectorGem()
{
}

void VectorGem::store_in_Vector()
{
	std::ifstream Mnfile;//open file
	std::ifstream Fnfile;
	std::ifstream usLastN;
		Mnfile.open("USCen/dist.male.first.txt");
		Fnfile.open("USCen/dist.female.first.txt");
		usLastN.open("USCen/US_LastNames.txt");

	//for (int index = 0; index < 1219; index++)
		for (int index = 0; index < 88799; index++)
	{//cout << "Reading from the file" << endl;
		Name_Sensus_C* temCon = new  Name_Sensus_C();

		Mnfile >> temCon->Male_Name;
		Mnfile >> temCon->M_C_fequency;
		Mnfile >> temCon->M_F_frequency;
		Mnfile >> temCon->m_Ind;


		Fnfile >> temCon->Female_Name;
		Fnfile >> temCon->F_C_fequency;
		Fnfile >> temCon->F_F_frequency;
		Fnfile >> temCon->F_Ind;

		usLastN >> temCon->Last_Name;
		usLastN >> temCon->L_C_fequency;
		usLastN >> temCon->L_F_frequency;
		usLastN >> temCon->L_Indl;


		vec_Name_Sensus_C.push_back(temCon);

	}

	Mnfile.close();
	usLastN.close();
	Fnfile.close();
}

void VectorGem::make_People()
{
	//Timer/memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------


	//store_in_Vector();

	for (int index = 0; index < 1000; index++)
	{
		sensus_People* p_container = new sensus_People();

			srand(index);

			int mindex,findex,lindex,genSelect;
			mindex = rand() % 1219 + 0;
			findex = rand() % 4275 + 0; 
			lindex = rand() % 88799 + 0;
			
			genSelect = rand() % 2 + 1;
			
			if (genSelect == 1)
			{
				p_container->first_Name = vec_Name_Sensus_C[mindex]->Male_Name;
				p_container->last_Name = vec_Name_Sensus_C[lindex]->Last_Name;
				people_vector.push_back(p_container);
			}
			else if (genSelect == 2)
			{
				p_container->first_Name = vec_Name_Sensus_C[findex]->Female_Name;
				p_container->last_Name = vec_Name_Sensus_C[lindex]->Last_Name;
				people_vector.push_back(p_container);
			}

			/*std::cout << people_vector[index]->first_Name << " " << people_vector[index]->last_Name << std::endl;*/
	}



	//--------------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;
}

void VectorGem::nameLookupF(std::string firstName)
{

	//Timer/memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------


	std::vector<sensus_People*>matchingNames;
	
	for (int index = 0; index < people_vector.size(); index++)
	{
		std::string tempFirstName = people_vector[index]->first_Name;
		
		if (firstName == tempFirstName)
			{
				matchingNames.push_back(people_vector[index]);
			}
	}

	for (int index = 0; index < matchingNames.size(); index++)
		{
			std::cout << matchingNames[index]->first_Name << "_" << matchingNames[index]->last_Name <<  std::endl;
		}

		if (matchingNames.size() == 0)
		{
			std::cout << "Sorry no Matches" << std::endl;
		}


		//--------------------------------------------------------
		timeObj.windows_MemeFunctions();
		timeObj.Stop();
		std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;
}

void VectorGem::nameLookupL(std::string lastname)
{
	//Timer/memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------

	std::vector<sensus_People*>matchingNames;

	for (int index = 0; index < people_vector.size(); index++)
	{
		std::string templastName = people_vector[index]->last_Name;
		
		if (lastname == templastName)
			{
				matchingNames.push_back(people_vector[index]);
			}
	}
		for (int index = 0; index < matchingNames.size(); index++)
			{
				std::cout << matchingNames[index]->first_Name << "_" << matchingNames[index]->last_Name << std::endl;
			}
			
		if (matchingNames.size() == 0)
				{
					std::cout << "Sorry no Matches" << std::endl;
				}

		//--------------------------------------------------------
		timeObj.windows_MemeFunctions();
		timeObj.Stop();
		std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;

}

void VectorGem::Lookup()
{
	std::cout << "Press 'F' to check for first names or Press 'L' to check for last names" << std::endl;
	std::cin >> typeSelection;
	if (typeSelection == 'F')
	{
		std::cout << "Lookup Names" << std::endl;
		std::cout << "Please input First name all in Uppercase" << std::endl;
		std::cout << "First name ->"; std::cin >> firstname;
		/*std::cout << "Last name ->"; std::cin >> lastname;*/
		std::cout << std::endl;
		nameLookupF(firstname);
	}
	else if (typeSelection == 'L')
	{
		std::cout << "Lookup Names" << std::endl;
		std::cout << "Please input Last name all in Uppercase" << std::endl;
		std::cout << "Last name ->"; std::cin >> lastname;
		/*std::cout << "Last name ->"; std::cin >> lastname;*/
		std::cout << std::endl;
		nameLookupL(lastname);
	}
}

bool nameSortLogic(sensus_People* C1_people, sensus_People* c2_people)
{
	return(C1_people->first_Name < c2_people->first_Name);
}

void VectorGem::sort_People_by_FirstNames()
{
	//Timer/memory
	timeObj.Start();
	timeObj.windows_MemeFunctions();
	//-------------------------------

	std::sort(people_vector.begin(), people_vector.end(),nameSortLogic );

	for (int index = 0; index < people_vector.size(); index++)
	{
		std::cout << people_vector[index]->first_Name << "_" << people_vector[index]->last_Name << std::endl;
	}

	//--------------------------------------------------------
	timeObj.windows_MemeFunctions();
	timeObj.Stop();
	std::cout << "Time elapsed :" << timeObj.GetElapsedSeconds() << std::endl;
}
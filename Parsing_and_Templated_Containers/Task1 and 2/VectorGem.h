#ifndef _VectorGem_
#define _VectorGem_

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <istream>

#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <algorithm>  //sort


#include "Timer_mem.h"

#include "Name_Sensus_C.h"
#include "sensus_People.h"

class VectorGem
{
public:
	VectorGem();
	~VectorGem();

	void store_in_Vector();
	void make_People();


	void Lookup();
	void nameLookupF(std::string firstName);
	void nameLookupL(std::string lastname);


	void sort_People_by_FirstNames();

	
	
private:

	std::vector<Name_Sensus_C*> vec_Name_Sensus_C;
	std::vector<sensus_People*> people_vector;
	
	std::string firstname, lastname;
	char typeSelection;


	CHRTimer timeObj;

};

#endif
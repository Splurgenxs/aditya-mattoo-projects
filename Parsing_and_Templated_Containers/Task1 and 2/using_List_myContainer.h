#pragma once
#ifndef _listUseContainer_
#define _listUseContainer_

#include <string>
#include "sensus_People.h"
#include "Name_Sensus_C.h"

#include "List_My_Container.h"
#include "Timer_mem.h"

class using_List_myContainer
{
public:
	using_List_myContainer();
	~using_List_myContainer();


	void store_in_list();
	void make_People();

	//------
	void Lookup();

	//--------------
	void sort_by_First_OR_LastNames();
	



	//Sort function 



private:

	List_MyContainer<Name_Sensus_C*> My_list_name_Sensus;
	List_MyContainer<sensus_People*> people_mylist;

	
	std::string firstname, lastname;
	char typeSelection;

	void nameLookupF(std::string firstName);
	void nameLookupL(std::string lastname);

	void bubble_Sort(std::string nameType);
	void swap(sensus_People* r, sensus_People* s);




	List_MyContainer<sensus_People*>matchingNamesL;
	List_MyContainer<sensus_People*> matchingNamesF;

	CHRTimer timeObj;

};

#endif
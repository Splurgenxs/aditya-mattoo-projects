#ifndef POWER_UP_H
#define POWER_UP_H

#include "Vertex.h"
#include <string.h>

class PowerUp
{
public:
	enum PowerUpType
	{
		WEAPON,
		ARMOUR,
		HEALTH
	};

	PowerUp(const char* name, Vertex position);
	virtual ~PowerUp();

	virtual PowerUp::PowerUpType GetPowerUpType() = 0;
    virtual Vertex GetPosition() = 0 ;

protected:
    Vertex      mPosition;
    PowerUpType mType;
    char*       mName;
};

#endif 

#include "PowerUp.h"

PowerUp::PowerUp(const char* name, Vertex position)
{
	mPosition = position;
	mName = new char[strlen(name)];
	strcpy(mName, name);
}

PowerUp::~PowerUp()
{
	delete mName;
}

PowerUp::PowerUpType PowerUp::GetPowerUpType()
{
	return(mType);
}

Vertex PowerUp::GetPosition()
{
	return(mPosition);
}
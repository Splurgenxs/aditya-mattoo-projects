#ifndef HEALTH_H
#define HEALTH_H

#include "PowerUp.h"

class Health : public PowerUp
{
public:

	Health(const char* name, Vertex position);
		~Health();

		virtual PowerUp::PowerUpType GetPowerUpType();
		virtual Vertex GetPosition();

 
protected:
    float healing;

	PowerUpType mType;
	Vertex mPosition;
};

#endif // HEALTH_H


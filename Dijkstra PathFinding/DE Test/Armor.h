#ifndef ARMOUR_H
#define ARMOUR_H

#include "PowerUp.h"

#include <stddef.h>
#include <string.h>

class Armor : public PowerUp
{
public:

	Armor(const char* name, const Vertex& position);
		~Armor();

	//-------------------------------->
	void set_DamageMitigate(int DamageMitigate);//one basic implementation
	int get_DamageMitigate();

	void set_HealthBuff();
	int get_HealthBuff();

	void set_RefelectDmg();
	int get_RefelectDmg();

	void set_MagicResistance();
	int get_MagicResistance();
 

	virtual PowerUp::PowerUpType GetPowerUpType();
	virtual Vertex GetPosition();
	//---------------------------------->

	const char* GetClanTag() const;

	void SetClanTag(char* n);

protected:

	PowerUpType mType;
	Vertex mPosition;
	char* mName;


    char* mClanTag;

	int DamageMitigate;
	int HealthBuff;
	int RefelectDmg;
	int MagicResistance;


};

#endif // ARMOUR_H


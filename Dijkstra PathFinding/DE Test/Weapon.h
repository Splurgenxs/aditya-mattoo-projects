#ifndef WEAPON_H
#define WEAPON_H

#include "PowerUp.h"

class Weapon : public PowerUp
{
public:


	Weapon(const char* name, Vertex position);
	~Weapon();

virtual PowerUp::PowerUpType GetPowerUpType();
virtual Vertex GetPosition();


	int get_lifeleech();//one basic implementation
	void set_lifeleech(int lifeleech);


	int	get_ManaDrain();
	void set_ManaDrain();


	int get_CritChance();
	void set_CritChance();


	int get_AttackSpeed();
	void set_AttackSpeed();

private:

	PowerUpType mType;
	Vertex mPosition;
	char* mName;


	int lifeleech;
	int	ManaDrain;
	int CritChance;
	int AttackSpeed;

};

#endif 


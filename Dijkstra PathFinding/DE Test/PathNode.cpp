#include "PathNode.h"



PathNode::PathNode(const char* name, Vertex position) : mPosition(position)
{
		mName = new char[strlen(name)];
		strcpy(mName, name);
}

PathNode::~PathNode()
{

}

bool PathNode::islink(PathNode *pathNode)
{
	for (int i = 0; i < mLinks.size(); i++)
	{
		if (mLinks[i]->GetName() == pathNode->GetName())
		{
			return true;
		}
	}
	return false;
}

void PathNode::AddLink(PathNode *pathNode)
{
	mLinks.push_back(pathNode);
}

void PathNode::RemoveLink(PathNode *pathNode)
{
	PathNodes::iterator i = std::find(mLinks.begin(), mLinks.end(), pathNode);
	mLinks.erase(i);
}

void PathNode::AddPowerUp(PowerUp *powerUp)
{
	mPowerUps.push_back(powerUp);
}

void PathNode::RemovePowerUp(PowerUp *powerUp)
{
	PowerUps::iterator i = std::find(mPowerUps.begin(), mPowerUps.end(), powerUp);
	mPowerUps.erase(i);
}

const char* PathNode::GetName() const
{
	return(mName);
}

const PathNodes& PathNode::GetLinks() const
{
	return(mLinks);
}

const PowerUps& PathNode::GetPowerUps() const
{
	return(mPowerUps);
}
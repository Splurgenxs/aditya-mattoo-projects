//'Dijkstra' , this algorith alowes for a fast search between nodes and finds the shortes path
//to the start and end point.
//although 'BFS''DFS' would do the same here (and sligtly faster),the absence of weights
//Removes the chance of adding additional huristics lateron, (would just waste more time later if need arised ,i think its a safe bet).

#include "Dijkstra.h"


Dijkstra::Dijkstra()
{

}
Dijkstra::Dijkstra(PathNodes spathnodes)
{
	this->djK_sPathnodes = spathnodes;
}
Dijkstra::~Dijkstra()
{
	delete Matrix_map;
}

void Dijkstra::initialize()//-->init 2d Array & setting weight (strting point as 0 , links as 1 , and the rest as infinity).
{
	Matrix_map = new int*[djK_sPathnodes.size()];//2d MAtrix alloc

	for (int i = 0; i < djK_sPathnodes.size(); i++)
		{

			Matrix_map[i] = new int[djK_sPathnodes.size()];//2d MAtrix alloc

		for (int j = 0; j < djK_sPathnodes.size(); j++)
			{
				if (djK_sPathnodes[i]->GetName() == djK_sPathnodes[j]->GetName())
				{
					Matrix_map[i][j] = 0; //initialise the first edge node as 0
				}
				else if (djK_sPathnodes[i]->islink(djK_sPathnodes[j]))
				{
					//(i) for the node ,(j) for the node that is linked to it
					Matrix_map[i][j] = 1; //all nodes with links get tagged as 1
				}
				else
				{
					Matrix_map[i][j] = INFINITY; // the rest as infinty
				}
			}
		}

	for (int i = 0; i< djK_sPathnodes.size(); i++)// init mark , predecessor and distance
		{
			mark.push_back(false);
			predecessor.push_back(-1);
			distance.push_back(INFINITY);
		}
	distance[source] = 0;//Distance from source node = 0(as this is the first node to start from).
	predecessor[source] = 0;//Has no predecessors as this is the first node from which traveral begins.

	//isPathAvailable = false;
}
    
int Dijkstra::getClosestUnmarkedNode()
{
	int minDistance = INFINITY;
	int closestUnmarkedNode;
	for (int i = 0; i < djK_sPathnodes.size(); i++)
	{
		if ((!mark[i]) && (minDistance >= distance[i]))
		{
			minDistance = distance[i];
			closestUnmarkedNode = i;
		}
	}
	return closestUnmarkedNode;
}

void Dijkstra::calculateDistance()
{
	//initialize();
	int minDistance = INFINITY;
	int closestUnmarkedNode;
	int count = 0;
	while (count < djK_sPathnodes.size())
	{
		closestUnmarkedNode = getClosestUnmarkedNode();
		mark[closestUnmarkedNode] = true;
		for (int i = 0; i<djK_sPathnodes.size(); i++)
		{
			if ((!mark[i]) && (Matrix_map[closestUnmarkedNode][i]>0))
			{

				if (distance[i] >= distance[closestUnmarkedNode] + Matrix_map[closestUnmarkedNode][i])
				{
					distance[i] = distance[closestUnmarkedNode] + Matrix_map[closestUnmarkedNode][i];
					predecessor[i] = closestUnmarkedNode;
				}
			}
		}
		count++;
	}
}

void Dijkstra::output(int x)
{
	if (x == source)
	{
		//cout << sPathNodes[x]->GetName() << "->";
		finalPath.push_back(djK_sPathnodes[x]);
	}
	else if (predecessor[x] == -1)
		return;
	else
	{
		output(predecessor[x]);
		//cout << sPathNodes[x]->GetName() << "->";
		finalPath.push_back(djK_sPathnodes[x]);
	}

}

bool Dijkstra::printPath(PowerUp::PowerUpType pUpType)//Checks for powerup type and eleminates unneeded path nodes.
{
	for (int i = 0; i < djK_sPathnodes.size(); i++)
	{
		output(i);
		if (finalPath[finalPath.size() - 1]->GetPowerUps().size() > 0)
		{
			if (finalPath[finalPath.size() - 1]->GetPowerUps()[0]->GetPowerUpType() == pUpType)
			{
				possiblePaths.push_back(finalPath);

			}
		}
		finalPath.clear();
		//cout << endl;
	}
	return (FinalPath());
}

bool Dijkstra::FinalPath()//Stores the final set of nodes that comprise of the path
{
	if (possiblePaths.size() == 0)
		return false;
	int min = 0;
	for (int i = 0; i < possiblePaths.size(); i++)
	{
		if (possiblePaths[i].size() < possiblePaths[min].size())
			min = i;
	}
	finalPath.clear();
	finalPath = possiblePaths[min];
	return true;
}

PathNodes Dijkstra::get_finalPath()
{
	return finalPath;
}

void Dijkstra::set_source(int source)
{
	this->source = source;
}
int Dijkstra::get_source()
{
	return source;
}
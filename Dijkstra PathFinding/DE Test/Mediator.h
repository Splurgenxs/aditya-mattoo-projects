#pragma once
#ifndef _mediator_
#define _mediator_


#include "Weapon.h"
#include "Health.h"
#include "Armor.h"

#include<iostream>
#include"Dijkstra.h"

class Mediator
{
public:
	Mediator();
	~Mediator();

	void create_Node_Struct();
	void initiate_Pathfinding();

private:

	PowerUps sPowerUps;
	int source;

	PathNodes sPathNodes;
	Dijkstra* Djx;

	bool FindPowerUp(PathNodes& path, PowerUp::PowerUpType mType, PathNode *start);
	// For this example, all links are symmetric.
	inline void LinkNodes(PathNode *n1, PathNode *n2);


	void takeUserInput();
	PowerUp::PowerUpType usrpwr;





};
#endif
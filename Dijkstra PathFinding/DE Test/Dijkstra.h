//'Dijkstra' , this algorith alowes for a fast search between nodes and finds the shortes path
//to the start and end point.
//although 'BFS''DFS' would do the same here (and sligtly faster),the absence of weights
//Removes the chance of adding additional huristics lateron, (would just waste more time later if need arised ,i think its a safe bet).


#ifndef _Dijkstra_
#define _Dijkstra_

#include "PathNode.h"
#include "PowerUp.h"
#include<vector>

#define INFINITY 9999

class Dijkstra
{
public:

	Dijkstra();
	Dijkstra(PathNodes spathnodes);
	~Dijkstra();

	PathNodes djK_sPathnodes;
void initialize();
int getClosestUnmarkedNode();
void calculateDistance();
bool printPath(PowerUp::PowerUpType);


PathNodes get_finalPath();

void set_source(int source);
int get_source();

//bool isPathAvailable;

private:

	int **Matrix_map;

	PathNodes finalPath;
	int source;

	std::vector<int> distance;
	std::vector<bool> mark; //keep track of visited node
	std::vector<int> predecessor;

	std::vector<PathNodes> possiblePaths;

	bool FinalPath();
	void output(int x);
};
#endif

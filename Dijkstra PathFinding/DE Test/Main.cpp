//==================================================================
//Copyright (C) 2015 Aditya Mattoo
//
//This program is free software; you can redistribute it and/or modify it under the terms of 
//the GNU General Public License as published by the Free Software Foundation; 
//either version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
//See the GNU General Public License for more details.
//==================================================================
 


#include<iostream>
#include "Mediator.h"





int main(int, char*[])
{

	Mediator med_Obj;
	med_Obj.create_Node_Struct();
	med_Obj.initiate_Pathfinding();
}



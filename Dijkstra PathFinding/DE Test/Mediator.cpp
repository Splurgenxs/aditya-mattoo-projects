#include "Mediator.h"


Mediator::Mediator()
{
}
Mediator::~Mediator()
{
	delete Djx;
}

bool Mediator::FindPowerUp(PathNodes& path, PowerUp::PowerUpType mType, PathNode *start)
{
	if (start->GetPowerUps().size()>0 && start->GetPowerUps()[0]->GetPowerUpType() == mType)//------>If same pUP_type return loc
	{
		path.push_back(start);
		return true;
	}

	Djx->initialize();// init 2d Array
	Djx->calculateDistance();

	if (Djx->printPath(mType))
	{
		path = Djx->get_finalPath();
		return true;
	}
	return(false); // No path found.
}

inline void Mediator::LinkNodes(PathNode *n1, PathNode *n2)
{
	n1->AddLink(n2);
	n2->AddLink(n1);
}

void Mediator::create_Node_Struct()
{
	sPathNodes.push_back(new PathNode("Node0", Vertex(300, 60, 0)));
	sPathNodes.push_back(new PathNode("Node1", Vertex(100, 60, 0)));
	sPathNodes.push_back(new PathNode("Node2", Vertex(80, 560, 0)));
	sPathNodes.push_back(new PathNode("Node3", Vertex(280, 650, 0)));
	sPathNodes.push_back(new PathNode("Node4", Vertex(300, 250, 0)));
	sPathNodes.push_back(new PathNode("Node5", Vertex(450, 400, 0)));
	sPathNodes.push_back(new PathNode("Node6", Vertex(450, 60, 0)));
	sPathNodes.push_back(new PathNode("Node7", Vertex(450, 400, 0)));

	LinkNodes(sPathNodes[1], sPathNodes[4]);
	LinkNodes(sPathNodes[0], sPathNodes[1]);
	LinkNodes(sPathNodes[0], sPathNodes[6]);
	LinkNodes(sPathNodes[0], sPathNodes[4]);
	LinkNodes(sPathNodes[7], sPathNodes[4]);
	LinkNodes(sPathNodes[7], sPathNodes[5]);
	LinkNodes(sPathNodes[2], sPathNodes[4]);
	LinkNodes(sPathNodes[2], sPathNodes[3]);
	LinkNodes(sPathNodes[3], sPathNodes[5]);

	sPowerUps.push_back(new Weapon("Weapon0", Vertex(340, 670, 0)));
	sPathNodes[3]->AddPowerUp(sPowerUps[0]);
	sPowerUps.push_back(new Weapon("Weapon1", Vertex(500, 220, 0)));
	sPathNodes[7]->AddPowerUp(sPowerUps[1]);

	sPowerUps.push_back(new Health("Health0", Vertex(490, 10, 0)));
	sPathNodes[6]->AddPowerUp(sPowerUps[2]);
	sPowerUps.push_back(new Health("Health1", Vertex(120, 20, 0)));
	sPathNodes[1]->AddPowerUp(sPowerUps[3]);

	sPowerUps.push_back(new Armor("Armour0", Vertex(500, 360, 0)));
	sPathNodes[5]->AddPowerUp(sPowerUps[4]);
	sPowerUps.push_back(new Armor("Armour1", Vertex(180, 525, 0)));
	sPathNodes[2]->AddPowerUp(sPowerUps[5]);

	takeUserInput();
}

void Mediator::takeUserInput()
{
	std::cout << "What node do you want to start with?(chose from 0 to 7) " << std::endl;
	std::cin >> source;
	if (source > 7 || source <= -1)
	{
		std::cout << "Invalid Input! \nPlease input a valid number\n\n";
		takeUserInput();
	}
	else
	{
		//--------------------------->
		Djx = new Dijkstra(sPathNodes);
		Djx->set_source(source);
		//---------------------------->

		int pwrSel;
		std::cout << "Select Power" << std::endl << " 1:for Health\n 2:for Armour\n 3:for Weapon \n\nThe Default selection is: Weapon" << std::endl;
		std::cin >> pwrSel;
		if (pwrSel > 3 || pwrSel <= 0)
		{ 
			std::cout << "Invalid Input! \nPlease input a valid number\n\n";
			takeUserInput();
		}
		else
		{
			switch (pwrSel)
			{
			case 1:
			{
				usrpwr = PowerUp::HEALTH;
			}
			break;
			case 2:
			{
				usrpwr = PowerUp::ARMOUR;
			}
			break;
			case 3:
			{
				usrpwr = PowerUp::WEAPON;
			}
			break;
			default:
			{
				usrpwr = PowerUp::WEAPON;
			}
			break;
			}
		}	
	}
}

void Mediator::initiate_Pathfinding()
{
	PathNodes path;
	if (!FindPowerUp(path, usrpwr, sPathNodes[source]))
	{
		printf("No path found: IMPOSSIBLE!\n");
	}
	else
	{
		printf("Path found: ");

		for (PathNodes::iterator i = path.begin(); i != path.end(); ++i)
		{
			PathNode *n = *i;
			printf("%s ", n->GetName());
		}
		printf("\n");
	}
}
#include "Health.h"


Health::Health(const char* name, Vertex position) : PowerUp(name, position)
{
	mType = HEALTH;
	this->mPosition = position;
}
Health::~Health()
{}

PowerUp::PowerUpType Health::GetPowerUpType()
{
	return(mType);
}
Vertex Health::GetPosition()
{
	return (mPosition);
}

#include "Armor.h"



Armor::Armor(const char* name, const Vertex& position) : PowerUp(name, position)
{
	mType = ARMOUR;
	mClanTag = NULL;
}

Armor::~Armor()
{
	delete mClanTag;
}


PowerUp::PowerUpType Armor::GetPowerUpType()
{
	return(mType);
}
Vertex Armor::GetPosition()
{
	return (mPosition);
}





void Armor::set_DamageMitigate(int DamageMitigate)//one basic implementation
{
	this->DamageMitigate = DamageMitigate;
}
int Armor::get_DamageMitigate()
{
	return DamageMitigate;
}

void Armor::set_HealthBuff()
{}
int Armor::get_HealthBuff()
{
	return HealthBuff;
}

void Armor::set_RefelectDmg()
{}
int Armor::get_RefelectDmg()
{
	return RefelectDmg;
}

void Armor::set_MagicResistance()
{}
int Armor::get_MagicResistance()
{
	return MagicResistance;
}

const char* Armor::GetClanTag() const
{
	return(mClanTag);
}

void Armor::SetClanTag(char* n)
{
	delete mClanTag;
	mClanTag = new char[strlen(n)];
	strcpy(mClanTag, n);
}
#ifndef PATH_NODE_H
#define PATH_NODE_H

#include "Vertex.h"

#include <vector>
#include <algorithm>

class PathNode;
typedef std::vector<PathNode*> PathNodes;

class PowerUp;
typedef std::vector<PowerUp*> PowerUps;

class PathNode
{
public:
	PathNode(const char* name, Vertex position);
	~PathNode();

//created to check if path node makes link with obj
	bool islink(PathNode *pathNode);
	void AddLink(PathNode *pathNode);
	void RemoveLink(PathNode *pathNode);
	void AddPowerUp(PowerUp *powerUp);
	void RemovePowerUp(PowerUp *powerUp);
	const char* GetName() const;
	const PathNodes& GetLinks() const;
	const PowerUps& GetPowerUps() const;

protected:
    Vertex      mPosition;
    char*       mName;

    PathNodes   mLinks;  //std::vector<PathNode*>
    PowerUps    mPowerUps;
};

#endif 
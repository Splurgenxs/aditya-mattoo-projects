Readme:

The Project Contains Level of detail toggling, with  object ID being switched on condition of distance.

Project also contains , Use of frame buffers to render screen buffer data onto a texture mapped on
all objects as per the setting of this project.

This project Also has the FBX model info "Vectors" directly loading into the buffer memory(as per my structure) roughly halving the time it took to load w.r.t the ply files
by circumventing the loop in the render callback.

Note:
Please Download Run and Play , has no Project Files as code is similar to another pushed project.
The loadUp take a few Seconds when it reads the High Poly FBX models.

Controls:
WASD to move the camera on the x and z axis
shift+W/S to move on the y axis.

Regards 
Aditya Mattoo
 

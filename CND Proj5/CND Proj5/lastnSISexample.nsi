!include "MUI2.nsh"
!include nsDialogs.nsh
!include "WordFunc.nsh"
!include "FileFunc.nsh"

;------------------------
!define APPNAME "CND_Project_5"
!define APPNAMEANDVERSION "${APPNAME}_v0.1"
;THe name of the installer
Name "${APPNAMEANDVERSION}"
Caption "${APPNAMEANDVERSION}"

;the installer out put file
OutFile "${APPNAMEANDVERSION}_Installer.exe"
InstallDir $DESKTOP\${APPNAME}

;Request access level
RequestExecutionLevel admin

;Show install Details
ShowInstDetails show
;-------------------
;Defines
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\orange-install.ico"
!define MUI_FINISHPAGE_TEXT "THansks for Installation"
;---------------------------------
;PAges
!insertmacro MUI_PAGE_WELCOME 
!insertmacro MUI_PAGE_LICENSE "Licence.txt"
;--------Custom Pages-------------
Page custom get_Email displayMD5Email
Page custom driveInfo

;---------------------------------
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY 
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
;---------------------------------
;LAnguages
!insertmacro MUI_LANGUAGE "English"
!insertmacro GetDrives
;---------------------------------
; Variables
Var Dialog
Var DataVar
Var Text

;Sections
Section "OpenGl Demo" OpenglDemo
;in order to create install dir
SetOutPath $INSTDIR
;write to registry.
WriteRegStr HKLM "Software\CND\" "Aditya Mattoo" "INFO0625"
WriteUninstaller "$INSTDIR\${APPNAMEANDVERSION}_Uninstaller.exe"

InitPluginsDir
File CND_Demo5.zip
ZipDLL::extractall "$INSTDIR\CND_Demo5.zip" "$INSTDIR"
Delete "$INSTDIR\CND_Demo5.zip"

SectionEnd

Section "Readme" ReadmeFiles
File Readme.txt
SectionEnd

Section "Create Shortcut on Desktop?" CreateDeskTopShortCut
;create desktop shortcut
CreateShortCut "$DESKTOP\${APPNAMEANDVERSION}.lnk" "$INSTDIR\W05D2_Graph.exe"
SectionEnd

Section "Uninstall"
;this method deletes everything
Delete "$INSTDIR\${APPNAMEANDVERSION}_Uninstaller.exe"
Delete "$INSTDIR\Readme.txt"
Delete "$DESKTOP\${APPNAMEANDVERSION}.lnk"
Delete "$INSTDIR\*.dll"
Delete "$INSTDIR\ply\*.ply"
Delete "$INSTDIR\*.glsl"
Delete "$INSTDIR\W05D2_Graph.exe "
RMDir  "$INSTDIR\ply"
RMDir  "$INSTDIR"


;DeleteRegKey HKLM "Software\CND\"
DeleteRegValue HKLM "Software\CND\" "Aditya Mattoo"
SectionEnd



;----------Functions----------------------
 Function .onInit
 MessageBox MB_OK|MB_ICONQUESTION "Please Close any Running Applications before Installing"

ReadRegStr $R0 HKLM "Software\CND\" "Aditya Mattoo"
  ${If} $R0 == "INFO0625"
  MessageBox MB_OK|MB_ICONQUESTION "${APPNAMEANDVERSION} seems to be already installed on your system.$\nPlease uninstall existing Application and try again.$\n  ~~Aborting Installation~~"
  Quit
${Endif}
FunctionEnd

Function driveInfo

	 StrCpy $R0 "0"
	 StrCpy $R1 "0"
	 StrCpy $R2 "0"
	 StrCpy $R3 "0"
	 StrCpy $R4 "0"
	 StrCpy $R5 "0"
	 StrCpy $R6 "0"
	 StrCpy $R7 "0"
	 StrCpy $R8 "0"
	 StrCpy $R9 "0"

	
	${GetDrives} "ALL" "GetDriveData"
	nsDialogs::Create 1018
	
	Pop $Dialog
	${If} $Dialog == error
		Abort
	${EndIf}
	Pop $0
	
	${NSD_CreateLabel}  0 20u 100% 20u "Listed Drives"
	Pop $DataVar
	
	${If} $R0 != "0"
		${NSD_CreateLabel}  0 40u 100% 20u "$R0 has a total size of $R1 GBs"
		Pop $DataVar
	${EndIf}
	
	${If} $R2 != "0"
		${NSD_CreateLabel}  0 60u 100% 20u "$R2 has a total size of $R3 GBs"
		Pop $DataVar
	${EndIf}
	
	${If} $R4 != "0"
		${NSD_CreateLabel}  0 80u 100% 20u "$R4 has a total size of $R5 GBs"
		Pop $DataVar
	${EndIf}
	
	${If} $R6 != "0"
		${NSD_CreateLabel}  0 100u 100% 20u "$R6 has a total size of $R7 GBs"
		Pop $DataVar
	${EndIf}
	
	${If} $R8 != "0"
		${NSD_CreateLabel}  0 120u 100% 20u "$R8 has a total size of $R9 GBs"
		Pop $DataVar
	${EndIf}
	nsDialogs::Show
	
FunctionEnd

Function GetDriveData

	${If} $R0 == "0"
		StrCpy $R0 $9
		${DriveSpace} "$9" "/D=T /S=G" $R1
		
	${ElseIf} $R2 == "0"
		StrCpy $R2 $9 
		${DriveSpace} "$9" "/D=T /S=G" $R3
		
	${ElseIf} $R4 == "0"
		StrCpy $R4 $9 
		${DriveSpace} "$9" "/D=T /S=G" $R5
		
	${ElseIf} $R6 == "0"
		StrCpy $R6 $9 
		${DriveSpace} "$9" "/D=T /S=G" $R7
		
	${ElseIf} $R8 == "0"
		StrCpy $R8 $9 
		${DriveSpace} "$9" "/D=T /S=G" $R9
	${EndIf}
	
	Push $0
FunctionEnd

Function get_Email
	nsDialogs::Create 1018
	
	${NSD_CreateLabel}  0 20u 100% 20u "Please Input a Valid Licence"
	Pop $DataVar
	
	${NSD_CreateText}  0 40u 100% 20u "Enter Email "
	Pop $Text
	

	
	nsDialogs::Show
FunctionEnd

Function displayMD5Email

${NSD_GetText} $Text $0
	md5dll::GetMD5String "$0"
	Pop $R1
	
	
	MessageBox MB_OK|MB_ICONQUESTION "$R1"

FunctionEnd

#ifndef _FILEDATA_LOADER_H
#define _FILEDATA_LOADER_H

#include "CFactoryMediator.h"
#include "CModelLoaderManager.h"
#include <sstream>
#include <fstream>
#include <iostream>

class FileDataLoader
{
public:
	FileDataLoader();
	~FileDataLoader();
	void loadModelsFromFile( CModelLoaderManager*  g_pModelLoader);
	void loadSceneFromFile(CFactoryMediator* pFactoryMediator, CModelLoaderManager*  g_pModelLoader,unsigned int &playerID);


private:
	std::vector< std::string > vecModelsToLoad;
	std::string modelname;
	std::string plypath;
	float rotx, roty, rotz;
	float posx, posy, posz;
	float scale,player;
	float colr, colg, colb;


};


#endif
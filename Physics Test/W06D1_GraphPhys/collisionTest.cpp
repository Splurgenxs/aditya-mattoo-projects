#include "collisionTest.h"

Collision::Collision()
{

}

Collision::~Collision()
{

}




void  Collision::SphereToEnvCollision(CGameObject* sphere , CGameObject* envObj ,std::vector<Physics::Point> &listOfClosestPt)
{
Physics::Point playerPos(sphere->position.x,sphere->position.y,sphere->position.z) ;
CSphere* s = (CSphere*)sphere;
    float distance = 99.0f; 
float radius = s->getRadius();

CVector3f offset = envObj->position;

std::vector<PLYVERTEX> m_verticies = envObj->getVecVerticesObj();
std::vector<PLYELEMENT> m_elements = envObj->getVecTrianglesObj();
std::vector<Physics::Point> closestPointsofObj ;
Physics::Point closestPoint ;
	
for(int i=0; i< m_elements.size(); i++ )
	{

		Physics::Point a(m_verticies[m_elements[i].vertex_index_1].xyz.x,
			             m_verticies[m_elements[i].vertex_index_1].xyz.y,
						 m_verticies[m_elements[i].vertex_index_1].xyz.z) ;

		Physics::Point b(m_verticies[m_elements[i].vertex_index_2].xyz.x,
			             m_verticies[m_elements[i].vertex_index_2].xyz.y,
						 m_verticies[m_elements[i].vertex_index_2].xyz.z) ;

		Physics::Point c(m_verticies[m_elements[i].vertex_index_3].xyz.x,
			             m_verticies[m_elements[i].vertex_index_3].xyz.y,
						 m_verticies[m_elements[i].vertex_index_3].xyz.z) ;

		//Adding points with offset 
		a += offset;
		b += offset;
		c += offset;
		
		bool bIsInsideFace = false;
	

	   Physics::Point newclosestPoint = Physics::ClosestPtPointTriangle(playerPos, a, b, c, bIsInsideFace );
	   float newDist = Physics::Distance(newclosestPoint,playerPos);
	   
	   if(newDist < distance)
		{
			closestPoint = newclosestPoint;
		    distance = newDist;

		
			if(distance <= radius)
			{
				closestPointsofObj.push_back(newclosestPoint);
			}
		  
	  
	
		}
			 
	}

			//Calculate actual closest dist of that object 
	if( closestPointsofObj.size() > 0)
	{
		Physics::Point sumObj(0.0f,0.0f,0.0f);
		for(int i = 0 ; i < closestPointsofObj.size(); i++)
		{
			sumObj += closestPointsofObj[i];
		}
		
		sumObj /= closestPointsofObj.size();
	
		listOfClosestPt.push_back(sumObj);
	}

}
void Collision::SphereToSphereCollision(CGameObject* sphere1 , CGameObject* sphere2,  std::vector<Physics::Point> &listOfClosestPt)
{
	// Proximity test for sphere to sphere
	Physics::Sphere a ;
	a.c = sphere1->position;
	a.vel = sphere1->velocity;
	a.r = ((CSphere*)sphere1)->getRadius() ;

	Physics::Sphere b ; 
	b.c = sphere2->position;
	b.r = ((CSphere*)sphere2)->getRadius();
	b.vel = sphere2->velocity; 
	
	Physics::Point closestPt;
	CVector3f newVel ; 
					
	if(Physics::ClosestPtSphereSphere( a,b, closestPt))
	{
		listOfClosestPt.push_back(closestPt);

		//Calculate change in velocity
		CVector3f newVelocityA ; 
	    CVector3f newVelocityB ; 
		newVelocityA.x = ((a.vel.x * (a.r - b.r)) + (2 * b.r * b.vel.x)) / (a.r + b.r);
		newVelocityA.y = ((a.vel.y * (a.r - b.r)) + (2 * b.r *b.vel.y)) / (a.r + b.r);
		newVelocityA.z = ((a.vel.z * (a.r - b.r)) + (2 * b.r * b.vel.z)) / (a.r + b.r);
					  
		sphere1->velocity = newVelocityA;

		newVelocityB.x = ((b.vel.x * (a.r - b.r)) + (2 * b.r * a.vel.x )) / (a.r + b.r);
		newVelocityB.y = ((b.vel.y  * (a.r - b.r)) + (2 * b.r *a.vel.y)) / (a.r + b.r);
		newVelocityB.z = ((b.vel.z  * (a.r - b.r)) + (2 * b.r * a.vel.z)) / (a.r + b.r);

		sphere2->velocity = newVelocityB;
	}
		
}

void Collision::CollisionCheckForSphere(std::vector< CGameObject* > vec_pGameObjects, int playerIndex)
{
	//check for collisions
	CSphere* sphere = (CSphere*)vec_pGameObjects[playerIndex];

	CVector3f playerPos = sphere->position;
	float radius = sphere->getRadius();

	std::vector<CVector3f> closestPointsInEnv ; //It contains list of closest points of all objects
	Physics::Point closestPoint ;
	     
	float distance = 99.0f; 

	 
	for(int j =0; j < vec_pGameObjects.size(); j++)
	{
		if(j == playerIndex)
		{
			continue;
		}

		if(vec_pGameObjects[j]->name != "SPHERE")
		{
			// Proximity test for sphere to other objects
			SphereToEnvCollision(vec_pGameObjects[playerIndex], vec_pGameObjects[j] , closestPointsInEnv);
		}
		else
		{
		    // Proximity test for sphere to sphere
			SphereToSphereCollision(vec_pGameObjects[playerIndex], vec_pGameObjects[j] , closestPointsInEnv);
		}
				
							
	}

	Physics::Point sumEnv(0.0f,0.0f,0.0f);

	for(int i = 0 ; i < closestPointsInEnv.size(); i++)
	{
		sumEnv += closestPointsInEnv[i];
	}
	
	sumEnv /= closestPointsInEnv.size();

		//if distance is less than radius bind the ball
	if(closestPointsInEnv.size() > 0 )
	{
		Physics::Point dist =  playerPos -sumEnv ;
	    CVector3f::Normalize( dist.x, dist.y,dist.z);
		
		dist = CVector3f::ScalarMultiply(dist , radius);
		

		Physics::Point distToTraverse = sumEnv + dist;
		vec_pGameObjects[playerIndex]->position =distToTraverse;// newPoint;
		
		
	}


}

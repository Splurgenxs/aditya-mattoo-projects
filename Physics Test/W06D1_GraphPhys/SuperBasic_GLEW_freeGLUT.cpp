// Some code originally from : http://openglbook.com/the-book.html 

// Comments are from Michael Feeney (mfeeney(at)fanshawec.ca)

#include "global.h"
#include "VertexTypes.h"

#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr
#include "fileDataLoader.h" 

FileDataLoader dataLoad;


//CHRTimer g_simTimer;


void LoadShaders(void);


int main(int argc, char* argv[])
{

	printTheWhatsThisProgramAboutBlurb();


	::OpenGL_Initialize( argc, argv, 1200, 800 );		// Initialize(argc, argv);

	LoadShaders();		// Moved from CreateCube


	// CModelLoaderManager
	g_pModelLoader = new CModelLoaderManager();

	//Loads all Models............
	dataLoad.loadModelsFromFile(g_pModelLoader);
	
	::g_SetShaderUniformVariables();

	g_pFactoryMediator = new CFactoryMediator();
	//Creates all Game Objects...........
	dataLoad.loadSceneFromFile(g_pFactoryMediator, g_pModelLoader, g_Player_ID);
	
	// Added October 3, 2014
	g_pCamera = new CCamera();
	// Camera expects an IMediator*, so cast it as that
	g_pCamera->SetMediator( (IMediator*)g_pFactoryMediator );

	g_pCamera->eye.x = 0.0f;		// Centred (left and right)
	g_pCamera->eye.y = 2.0f;		// 2.0 units "above" the "ground"
	g_pCamera->eye.z = 4.0f;		// 4.0 units "back" from the origin

	//g_pCamera->target    CVector3f gets set to 0,0,0

	g_pCamera->up.x = 0.0f;
	g_pCamera->up.y = 1.0f;				// The Y axis is "up and down"


	// Set up the basic lighting
	::g_ShaderUniformVariables.Lights[0].isEnabled = GL_TRUE;
	::g_ShaderUniformVariables.Lights[0].isLocal = GL_TRUE;		// Only directional lights AREN'T local
	::g_ShaderUniformVariables.Lights[0].isSpot = GL_FALSE;		// Local = TRUE, Spot = FALSE --- Point light

	::g_ShaderUniformVariables.Lights[0].color = glm::vec3(0.0f, 0.0f, 3.0f);		// White light
	::g_ShaderUniformVariables.Lights[0].ambient = glm::vec3(0.0f, 0.0f, 0.3f);	// White light
	::g_ShaderUniformVariables.Lights[0].position = glm::vec3(0.0f, 2.0f, 0.0f);	// Just above the origin
	::g_ShaderUniformVariables.Lights[0].halfVector = glm::vec3(1.0f, 1.0f, 1.0f); // = normalize( lightDirection + EyeDirection ); Only used for directional light
	//::g_ShaderUniformVariables.Lights[0].coneDirection =glm::vec3(0.0f, 0.0f, 0.0f); // Only used for spot lights
	//::g_ShaderUniformVariables.Lights[0].spotCosCutoff =5.0f; // Only used for spot lights
	//::g_ShaderUniformVariables.Lights[0].spotExponent = 1.0f;// Only used for spot lights

	::g_ShaderUniformVariables.Lights[0].constantAttenuation = 0.1f;
	::g_ShaderUniformVariables.Lights[0].linearAttenuation = 0.1f;
	::g_ShaderUniformVariables.Lights[0].quadraticAttenuation = 0.08f;


	
	// Set up the basic lighting
	::g_ShaderUniformVariables.Lights[1].isEnabled = GL_TRUE;
	::g_ShaderUniformVariables.Lights[1].isLocal = GL_FALSE;		// Only directional lights AREN'T local
	::g_ShaderUniformVariables.Lights[1].isSpot = GL_FALSE;		// Local = TRUE, Spot = FALSE --- Point light

	::g_ShaderUniformVariables.Lights[1].color = glm::vec3(0.3f, 0.0f, 0.0f);		// White light
	::g_ShaderUniformVariables.Lights[1].ambient = glm::vec3(0.1f, 0.1f, 0.1f);	// White light
	::g_ShaderUniformVariables.Lights[1].position = glm::vec3(2.0f, 0.0f, 0.0f);	// Just above the origin
	//::g_ShaderUniformVariables.Lights[0].halfVector = .... // = normalize( lightDirection + EyeDirection ); Only used for directional light
	::g_ShaderUniformVariables.Lights[1].coneDirection =glm::vec3(1.0f, 2.0f, 0.0f); // Only used for spot lights
	::g_ShaderUniformVariables.Lights[1].spotCosCutoff =0.1f; // Only used for spot lights
	::g_ShaderUniformVariables.Lights[1].spotExponent = 0.8f;// Only used for spot lights

//	::g_ShaderUniformVariables.Lights[1].constantAttenuation = -0.1f;
//	::g_ShaderUniformVariables.Lights[1].linearAttenuation = 0.0f;
//	::g_ShaderUniformVariables.Lights[1].quadraticAttenuation = 0.0f;

	::g_ShaderUniformVariables.Lights[2].isEnabled = GL_TRUE;
	::g_ShaderUniformVariables.Lights[2].isLocal = GL_FALSE;		// Only directional lights AREN'T local
	::g_ShaderUniformVariables.Lights[2].isSpot = GL_FALSE;		// Local = TRUE, Spot = FALSE --- Point light

	::g_ShaderUniformVariables.Lights[2].color = glm::vec3(0.0f, 0.3f, 0.0f);		// White light
	::g_ShaderUniformVariables.Lights[2].ambient = glm::vec3(0.1f, 0.1f, 0.1f);	// White light
	::g_ShaderUniformVariables.Lights[2].position = glm::vec3(-2.0f, 0.0f, 0.0f);	// Just above the origin
	//::g_ShaderUniformVariables.Lights[0].halfVector = .... // = normalize( lightDirection + EyeDirection ); Only used for directional light
	::g_ShaderUniformVariables.Lights[2].coneDirection =glm::vec3(1.0f, 2.0f, 0.0f); // Only used for spot lights
	::g_ShaderUniformVariables.Lights[2].spotCosCutoff =0.1f; // Only used for spot lights
	::g_ShaderUniformVariables.Lights[2].spotExponent = 0.8f;// Only used for spot lights


	// Added in animation on Sept 19
	g_simTimer.Reset();
	g_simTimer.Start();		// Start "counting"

	glutMainLoop();
  
	exit(EXIT_SUCCESS);
}



void LoadShaders(void)
{
	
	// A "Shader Program" is a container to hold a "set" of shaders  (at least a vertex and a fragment shader, but possibly more)
	g_ShaderProgram_ID = glCreateProgram();	


	ExitOnGLError("ERROR: Could not create the shader program");

	g_VertexShader_ID = LoadShader("shaders/OpenGLProgGuidMutipleLights.vertex.glsl", GL_VERTEX_SHADER);
	g_FragmentShader_ID = LoadShader("shaders/OpenGLProgGuidMutipleLights.fragment.glsl", GL_FRAGMENT_SHADER);


	// Now we associate these specific COMPILED shaders to the "program"...
	glAttachShader( g_ShaderProgram_ID, g_VertexShader_ID );
	glAttachShader( g_ShaderProgram_ID, g_FragmentShader_ID );

	glLinkProgram( g_ShaderProgram_ID );

	ExitOnGLError("ERROR: Could not link the shader program");


  return;
}




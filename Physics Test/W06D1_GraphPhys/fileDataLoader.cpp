#include "fileDataLoader.h"

FileDataLoader::FileDataLoader()
{

}

FileDataLoader::~FileDataLoader()
{

}

void FileDataLoader::loadModelsFromFile( CModelLoaderManager*  g_pModelLoader)
{
	std::ifstream myFile("external_file/model.txt");
	// Did it open?
	if (!myFile.is_open())
	{
		std::cout << "Can't open the file. Sorry it didn't work out.";
	}
	std::string mytext;
	
	while (myFile >> mytext)
	{
		if (mytext == "start")
			break;

	}



	int count = 0;
	while (!myFile.eof())
	{
		myFile >> plypath;
		if(plypath == "end")
			break;
		vecModelsToLoad.push_back(plypath);
		count++;
		
	}

	if ( ! g_pModelLoader->LoadModels( vecModelsToLoad ) )
	{
		std::cout << "Can't load one or more models. Sorry it didn't work out." << std::endl;
		//return -1;
	}else{
	
	std::cout << "Models loaded :" <<count<< std::endl;
	}



}

void FileDataLoader::loadSceneFromFile(CFactoryMediator* pFactoryMediator, CModelLoaderManager*  g_pModelLoader,unsigned  int &playerID)
{
	std::ifstream daFile("external_file/scene.txt");
	// Did it open?
	if (!daFile.is_open())
	{
		std::cout << "Can't open the file. Sorry it didn't work out.";
	}
	std::string temptext;
	CPlyInfo tempPlyInfo;
	while (daFile >> temptext)
	{
		if (temptext == "start")
			break;

	}
		int count = 0;
		while (!daFile.eof())
		{
			
			daFile >> modelname;
			

			if(modelname == "end")
				break;
		
			daFile >> plypath;
			daFile >> rotx >> roty >> rotz;
			daFile >> posx >> posy >> posz;
			daFile >> colr >> colg >> colb ;
			daFile >> scale ;
			daFile >> player;
		

		unsigned int ID = pFactoryMediator->CreateObjectByType(modelname,plypath);
		pFactoryMediator->UpdateObjectRotationByID(ID, CVector3f(rotx,roty,rotz));
		pFactoryMediator->UpdateObjectPositionByID(ID, CVector3f(posx,posy,posz));
		pFactoryMediator->UpdateColourByID(ID, CVector3f(colr,colg,colb));
		pFactoryMediator->setElementsByID(ID,plypath, g_pModelLoader);
		g_pModelLoader->GetRenderingInfoByModelFileName(plypath, tempPlyInfo);
		pFactoryMediator->UpdateObjectScaleByID(ID, scale / tempPlyInfo.extent);
		if(modelname == "Sphere"){
		pFactoryMediator->UpdateObjectRadiusByID(ID,scale/ 2 );
		}

		if(player == 1)
		{
		playerID = ID;
		}
		count++;	
		std::cout <<"Game Object "<< modelname <<" "<<count<<" created"<< std::endl;
	   }
		


}


#include "global.h" 
#include "collisionTest.h"



static const float g_MAXIMUM_TIME_STEP = 0.1f;		// 10th of a second or 100ms 



void IdleFunction(void)
{
	// Update the "simulation" at this point.
	// 1. Get elapsed time
	g_simTimer.Stop();
	float deltaTime = ::g_simTimer.GetElapsedSeconds();
	// Is the number TOO big (we're in a break point or something?)
	if ( deltaTime > g_MAXIMUM_TIME_STEP )
	{	// Yup, so clamp it to a new value
		deltaTime = g_MAXIMUM_TIME_STEP;
	}
	g_simTimer.Start();
	Collision c;   // <- this handles all collisions
	// Set the light at the same location as the player object...
	/*CVector3f playerLocation;
	g_pFactoryMediator->GetPositionByID( ::g_Player_ID, playerLocation );

	::g_ShaderUniformVariables.Lights[0].position = glm::vec3(playerLocation.x, 
		                                                      playerLocation.y + 2.0f,
															  playerLocation.z );

	*/


	std::vector< CGameObject* > vec_pGameObjects;
	g_pFactoryMediator->getPhysicsObjects( vec_pGameObjects );
	float acc = 0.5f;
	for ( int index = 0; index != static_cast<int>( vec_pGameObjects.size() ); index++ )
	{
		//////Acceleration 
		//if(vec_pGameObjects[index]->velocity.x < 0.75)
		//vec_pGameObjects[index]->velocity.x +=( vec_pGameObjects[index]->acceleration.x * deltaTime ); 

		//if(vec_pGameObjects[index]->velocity.y < 0.75)
		//vec_pGameObjects[index]->velocity.y +=( (vec_pGameObjects[index]->acceleration.y) * deltaTime );

		//if(vec_pGameObjects[index]->velocity.z < 0.75)
		//vec_pGameObjects[index]->velocity.z +=( vec_pGameObjects[index]->acceleration.z * deltaTime ); 

		//Gravity on all spheres....
		if(vec_pGameObjects[index]->name=="SPHERE"){
		vec_pGameObjects[index]->position.y -= ( acc * deltaTime );
		}


		//Displacement
		vec_pGameObjects[index]->position.x += ( vec_pGameObjects[index]->velocity.x * deltaTime ); 
		vec_pGameObjects[index]->position.y += ( vec_pGameObjects[index]->velocity.y * deltaTime );
		vec_pGameObjects[index]->position.z += ( vec_pGameObjects[index]->velocity.z * deltaTime );
	
		

		//Check type of object (currently collisions are handled only for spheres)
		if(vec_pGameObjects[index]->name=="SPHERE")
		{
			c.CollisionCheckForSphere(vec_pGameObjects, index);
		}


		//to re-incarnate the objects if they falls into the abyss 
		if(vec_pGameObjects[index]->position.y < -3){
			vec_pGameObjects[index]->position = CVector3f(0.5,3.0,0.0);
		    vec_pGameObjects[index]->velocity = CVector3f();
		}

	}


	for ( int index = 0; index != static_cast<int>( vec_pGameObjects.size() ); index++ )
	{
		vec_pGameObjects[index]->Update(deltaTime);
	}

	
	
	::g_pCamera->Update( deltaTime );


	glutPostRedisplay();
	return;
}

void TimerFunction(int Value)
{
  if (0 != Value) 
  {


	std::stringstream ss;
	ss << WINDOW_TITLE_PREFIX << ": " 
		<< ::g_FrameCount * 4 << " Frames Per Second @ "
		<< ::g_screenWidth << " x"				// << CurrentWidth << " x "
		<< ::g_screenHeight;					// << CurrentHeight;

    glutSetWindowTitle(ss.str().c_str());
    //glutSetWindowTitle(TempString);
    //free(TempString);
  }

  ::g_FrameCount = 0;	// FrameCount = 0;
  glutTimerFunc(250, TimerFunction, 1);

  return;
}
#ifndef _COLLIDE_H
#define _COLLIDE_H

#include "global.h"


class Collision
{
public:
	Collision();
	~Collision();


void CollisionCheckForSphere(std::vector< CGameObject* > vec_pGameObjects, int playerIndex);

// Called to check Sphere collisions
void SphereToEnvCollision(CGameObject* sphere , CGameObject* envObj, std::vector<Physics::Point> &listOfClosestPt);
void SphereToSphereCollision(CGameObject* sphere1 , CGameObject* sphere2, std::vector<Physics::Point> &listOfClosestPt);
//.......................


};


#endif
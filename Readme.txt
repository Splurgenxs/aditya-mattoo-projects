__________                   .___              
\______   \ ____ _____     __| _/_____   ____  
 |       _// __ \\__  \   / __ |/     \_/ __ \ 
 |    |   \  ___/ / __ \_/ /_/ |  Y Y  \  ___/ 
 |____|_  /\___  >____  /\____ |__|_|  /\___  >
        \/     \/     \/      \/     \/     \/ 

Created By Aditya Mattoo 
Personal and Academic Projects

You can pull the Whole Project Reposotory from here.
Partof/Extends the 'Aditya Mattoo' programmer project portfolio. 
//==================================================================
All Projects are under Copyright (C) 2013 Aditya Mattoo

This program is free software; you can redistribute it and/or modify it under the terms of 
the GNU General Public License as published by the Free Software Foundation; 
either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.
//==================================================================
  
Note:
Projects are made on VS2013 and Havok Projects on Vs2012. Some effort is needed to get them running.
This is to demonstrate project work to anyone determined to find out more, 
all projects are susceptible to NOT work on your local environments as SDKs and libs are 
huge (for HAVOK & FBX) and i have not uploaded them; also the referencing directories will be different as well.

2017: Havok SDK has no longer free, a better free alternative would be Bullet Physics


Regards =D